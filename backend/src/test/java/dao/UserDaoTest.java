package dao;

import domain.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import util.DatabaseCleaner;
import util.Location;
import util.enumerations.UserType;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class UserDaoTest {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestGovernment_PU");
    private EntityManager em;
    private EntityTransaction tx;

    private UserDaoImpl userDao = new UserDaoImpl();

    private User user1 = new User("user1", "testUser1","email@test.com", "password", new Date(System.currentTimeMillis()), UserType.CUSTOMER, new Location("5612MA", "Rachelsmolen 1", "Eindhoven"));
    private User user2 = new User("user2", "testUser2","email@test.com", "password", new Date(System.currentTimeMillis()), UserType.CUSTOMER, new Location("1056 ST", "Vespuccistraat 106", "Amsterdam"));
    private User user3 = new User("user3", "testUser3","email@test.com", "password", new Date(System.currentTimeMillis()), UserType.ADMIN, new Location("4384 GM", "Johan Melchior Kemperstraat 69", "Vlissingen"));
    private User user4 = new User("user4", "testUser4","email@test.com", "password", new Date(System.currentTimeMillis()), UserType.ADMIN, new Location("5914 RT", "Bachstraat 168", "Venlo"));
    private User user5 = new User("user5", "testUser5","email@test.com", "password", new Date(System.currentTimeMillis()), UserType.EMPLOYEE, new Location("5802 EJ", "Plataanstraat 166", "Venray"));

    private List<User> allUsers = new ArrayList<>();

    @Before
    public void setUp(){
        try {
            new DatabaseCleaner(emf.createEntityManager()).clean();
        } catch (SQLException ex) {

        }

        em = emf.createEntityManager();
        tx = em.getTransaction();
        userDao.em = em;

        em.getTransaction().begin();
        userDao.create(user1);
        userDao.create(user2);
        userDao.create(user3);
        userDao.create(user4);
        em.getTransaction().commit();
        allUsers.add(user1);
        allUsers.add(user2);
        allUsers.add(user3);
        allUsers.add(user4);
    }

    @After
    public void breakDown(){
    }

    @Test
    public void testCount(){

        assertEquals(4, userDao.count());
    }

    @Test
    public void testCreate(){
        assertEquals(4, userDao.count());
        em.getTransaction().begin();
        userDao.create(user5);
        em.getTransaction().commit();
        assertEquals(5, userDao.count());
    }

    @Test
    public void testEdit(){
        em.getTransaction().begin();
        user2.setEmail("test");
        userDao.edit(user2);
        em.getTransaction().commit();
        user3 = userDao.findById(user2.getId());
        assertEquals(user3.getEmail(), user2.getEmail());
    }

    @Test
    public void testRemove(){
        assertEquals(4, userDao.count());
        em.getTransaction().begin();
        userDao.remove(user4);
        em.getTransaction().commit();
        assertEquals(3, userDao.count());
    }

    @Test
    public void testFindAll(){
        List<User> result = (List<User>) userDao.findAll();
        assertEquals(4, result.size());
        assertTrue(allUsers.contains(result.toArray()[0]));
        assertTrue(allUsers.contains(result.toArray()[1]));
        assertTrue(allUsers.contains(result.toArray()[2]));
        assertTrue(allUsers.contains(result.toArray()[3]));
    }

    @Test
    public void testFindById(){
        User testM1 = userDao.findById(user1.getId());
        User testM2 = userDao.findById(user2.getId());
        User testM3 = userDao.findById(user3.getId());
        assertEquals(testM1, user1);
        assertEquals(testM2, user2);
        assertEquals(testM3, user3);
    }

    @Test
    public void testFindByUsername(){
        User testM1 = userDao.findByUsername(user1.getUsername());
        User testM2 = userDao.findByUsername(user2.getUsername());
        User testM3 = userDao.findByUsername(user3.getUsername());
        assertEquals(testM1, user1);
        assertEquals(testM2, user2);
        assertEquals(testM3, user3);
    }

}
