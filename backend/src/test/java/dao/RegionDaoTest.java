package dao;

import domain.Region;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import util.DatabaseCleaner;
import util.Position;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class RegionDaoTest {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestGovernment_PU");
    private EntityManager em;
    private EntityTransaction tx;

    private RegionDaoImpl regionDao = new RegionDaoImpl();

    private Region region1 = new Region("Limburg", 1.2, new Position(0.0f, 0.1f), new Position(0.2f, 0.3f));
    private Region region2 = new Region("Venray", 1.3, new Position(0.0f, 0.1f), new Position(0.2f, 0.3f));
    private Region region3 = new Region("Zeeland", 1.0, new Position(0.0f, 0.1f), new Position(0.2f, 0.3f));
    private Region region4 = new Region("Koelie", 1.8, new Position(0.0f, 0.1f), new Position(0.2f, 0.3f));

    private List<Region> allRegions = new ArrayList<>();

    @Before
    public void setUp(){
        try {
            new DatabaseCleaner(emf.createEntityManager()).clean();
        } catch (SQLException ex) {

        }

        em = emf.createEntityManager();
        tx = em.getTransaction();
        regionDao.em = em;

        em.getTransaction().begin();
        regionDao.create(region1);
        regionDao.create(region2);
        regionDao.create(region3);
        em.getTransaction().commit();
        allRegions.add(region1);
        allRegions.add(region2);
        allRegions.add(region3);
    }

    @After
    public void breakDown(){
    }

    @Test
    public void testCount(){

        assertEquals(3, regionDao.count());
    }

    @Test
    public void testCreate(){
        assertEquals(3, regionDao.count());
        em.getTransaction().begin();
        regionDao.create(region4);
        em.getTransaction().commit();
        assertEquals(4, regionDao.count());
    }

    @Test
    public void testEdit(){
        em.getTransaction().begin();
        region1.setBasePrice(2.0);
        regionDao.edit(region1);
        em.getTransaction().commit();
        region2 = regionDao.findById(region1.getId());
        assertEquals(region1.getBasePrice(), region2.getBasePrice());
    }

    @Test
    public void testRemove(){
        assertEquals(3, regionDao.count());
        em.getTransaction().begin();
        regionDao.remove(region3);
        em.getTransaction().commit();
        assertEquals(2, regionDao.count());
    }

    @Test
    public void testFindAll(){
        List<Region> result = (List<Region>) regionDao.findAll();
        assertEquals(3, result.size());
        assertTrue(allRegions.contains(result.toArray()[0]));
        assertTrue(allRegions.contains(result.toArray()[1]));
        assertTrue(allRegions.contains(result.toArray()[2]));
    }

    @Test
    public void testFindById(){
        Region testM1 = regionDao.findById(region1.getId());
        Region testM2 = regionDao.findById(region2.getId());
        Region testM3 = regionDao.findById(region3.getId());
        assertEquals(testM1, region1);
        assertEquals(testM2, region2);
        assertEquals(testM3, region3);
    }

    @Test
    public void testFindByUsername(){
        Region testM1 = regionDao.findByName(region1.getName());
        Region testM2 = regionDao.findByName(region2.getName());
        Region testM3 = regionDao.findByName(region3.getName());
        assertEquals(testM1, region1);
        assertEquals(testM2, region2);
        assertEquals(testM3, region3);
    }

}
