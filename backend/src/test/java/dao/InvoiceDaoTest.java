package dao;

import domain.Invoice;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import util.DatabaseCleaner;
import util.enumerations.InvoiceStatus;
import util.enumerations.PaymentStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class InvoiceDaoTest {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestGovernment_PU");
    private EntityManager em;
    private EntityTransaction tx;

    private InvoiceDaoImpl invoiceDao = new InvoiceDaoImpl();

    private Invoice invoice1 = new Invoice(1L, 10.50, InvoiceStatus.SUCCESS, PaymentStatus.OPEN, new Date(System.currentTimeMillis()),  new Date(System.currentTimeMillis() + 28400000));
    private Invoice invoice2 = new Invoice(1L, 10.50, InvoiceStatus.WARNING, PaymentStatus.FULFILLED, new Date(System.currentTimeMillis()),  new Date(System.currentTimeMillis() + 28400000));
    private Invoice invoice3 = new Invoice(2L, 11.50, InvoiceStatus.PENDING, PaymentStatus.OPEN, new Date(System.currentTimeMillis()),  new Date(System.currentTimeMillis() + 28400000));
    private Invoice invoice4 = new Invoice(2L, 11.50, InvoiceStatus.FAILED, PaymentStatus.CANCELLED, new Date(System.currentTimeMillis()),  new Date(System.currentTimeMillis() + 28400000));

    private List<Invoice> allInvoices = new ArrayList<>();

    @Before
    public void setUp(){
        try {
            new DatabaseCleaner(emf.createEntityManager()).clean();
        } catch (SQLException ex) {

        }

        em = emf.createEntityManager();
        tx = em.getTransaction();
        invoiceDao.em = em;

        em.getTransaction().begin();
        invoiceDao.create(invoice1);
        invoiceDao.create(invoice2);
        invoiceDao.create(invoice3);
        em.getTransaction().commit();
        allInvoices.add(invoice1);
        allInvoices.add(invoice2);
        allInvoices.add(invoice3);
    }

    @Test
    public void testCount(){
        assertEquals(3L, invoiceDao.count());
    }

    @Test
    public void testCreate(){
        em.getTransaction().begin();
        invoiceDao.create(invoice4);
        em.getTransaction().commit();
        assertEquals(4L, invoiceDao.count());
    }

    @Test
    public void testEdit(){
        em.getTransaction().begin();
        invoice1.setPaymentStatus(PaymentStatus.FULFILLED);
        invoiceDao.edit(invoice1);
        em.getTransaction().commit();
        invoice2 = invoiceDao.findById(invoice1.getId());
        assertEquals(invoice1.getPaymentStatus(), invoice2.getPaymentStatus());
    }

    @Test
    public void testRemove(){
        assertEquals(3, invoiceDao.count());
        em.getTransaction().begin();
        invoiceDao.remove(invoice3);
        em.getTransaction().commit();
        assertEquals(2, invoiceDao.count());
    }

    @Test
    public void testFindAll(){
        List<Invoice> result = (List<Invoice>) invoiceDao.findAll();
        assertEquals(3, result.size());
        assertTrue(allInvoices.contains(result.toArray()[0]));
        assertTrue(allInvoices.contains(result.toArray()[1]));
        assertTrue(allInvoices.contains(result.toArray()[2]));
    }

    @Test
    public void testFindById(){
        Invoice testM1 = invoiceDao.findById(invoice1.getId());
        Invoice testM2 = invoiceDao.findById(invoice2.getId());
        Invoice testM3 = invoiceDao.findById(invoice3.getId());
        assertEquals(testM1, invoice1);
        assertEquals(testM2, invoice2);
        assertEquals(testM3, invoice3);
    }

    @Test
    public void testFindByUser(){
        assertEquals(2,invoiceDao.findByCustomerId(1L).size());
        assertEquals(1,invoiceDao.findByCustomerId(2L).size());
    }

    @Test
    public void testFindByPaymentStatus(){
        assertEquals(2,invoiceDao.findByStatus(PaymentStatus.OPEN).size());
        assertEquals(0,invoiceDao.findByStatus(PaymentStatus.CANCELLED).size());
        assertEquals(1,invoiceDao.findByStatus(PaymentStatus.FULFILLED).size());
    }

    @Test
    public void testFilterQueryUserId() {
        String query = "SELECT i.* FROM INVOICE i WHERE user_id = 1";
        Collection<Invoice> result = invoiceDao.filterQuery(query);
        assertTrue(result.contains(invoice1));
        assertTrue(result.contains(invoice2));
    }

    @Test
    public void testFilterQueryMinAmount() {
        String query = "SELECT i.* FROM INVOICE i WHERE total_amount > 11";
        Collection<Invoice> result = invoiceDao.filterQuery(query);
        assertTrue(result.contains(invoice3));
    }

    @Test
    public void testFilterQueryMaxAmount() {
        String query = "SELECT i.* FROM INVOICE i WHERE total_amount < 11";
        Collection<Invoice> result = invoiceDao.filterQuery(query);
        assertTrue(result.contains(invoice1));
        assertTrue(result.contains(invoice2));
    }

    @Test
    public void testFilterQueryStatus() {
        String query = "SELECT i.* FROM INVOICE i WHERE status = \"SUCCESS\"";
        Collection<Invoice> result = invoiceDao.filterQuery(query);
        assertTrue(result.contains(invoice1));
    }

    @Test
    public void testFilterQueryPaymentStatus() {
        String query = "SELECT i.* FROM INVOICE i WHERE payment_status = \"OPEN\"";
        Collection<Invoice> result = invoiceDao.filterQuery(query);
        assertTrue(result.contains(invoice1));
        assertTrue(result.contains(invoice3));
    }
}
