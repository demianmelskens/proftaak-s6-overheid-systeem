package dao;

import domain.Invoice;
import domain.InvoiceComplaint;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import util.DatabaseCleaner;
import util.enumerations.InvoiceStatus;
import util.enumerations.PaymentStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class InvoiceComplaintDaoImplTest {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestGovernment_PU");
    private EntityManager em;
    private EntityTransaction tx;

    private InvoiceComplaintDaoImpl invoiceComplaintDao = new InvoiceComplaintDaoImpl();
    private InvoiceDaoImpl invoiceDao = new InvoiceDaoImpl();

    private Invoice invoice1 = new Invoice(1L, 10.50, InvoiceStatus.SUCCESS, PaymentStatus.OPEN, new Date(System.currentTimeMillis()),  new Date(System.currentTimeMillis() + 28400000));
    private Invoice invoice2 = new Invoice(1L, 10.50, InvoiceStatus.WARNING, PaymentStatus.FULFILLED, new Date(System.currentTimeMillis()),  new Date(System.currentTimeMillis() + 28400000));

    private InvoiceComplaint invoiceComplaint1 = new InvoiceComplaint("is niet goed he", invoice1);
    private InvoiceComplaint invoiceComplaint2 = new InvoiceComplaint("jeroen doe dat nou niet!", invoice2);

    @Before
    public void setUp() throws Exception {
        try {
            new DatabaseCleaner(emf.createEntityManager()).clean();
        } catch (SQLException ex) {

        }

        em = emf.createEntityManager();
        tx = em.getTransaction();
        invoiceComplaintDao.em = em;
        invoiceDao.em = em;

        em.getTransaction().begin();
        em.persist(invoiceComplaint1);
        em.persist(invoiceComplaint2);
        invoice1.setComplaints(new ArrayList<InvoiceComplaint>() {{ add(invoiceComplaint1); }});
        invoice2.setComplaints(new ArrayList<InvoiceComplaint>() {{ add(invoiceComplaint2); }});
        invoiceDao.create(invoice1);
        invoiceDao.create(invoice2);
        em.getTransaction().commit();
    }

//    @Test
//    public void getComplaint() {
//        InvoiceComplaint actual = invoiceComplaintDao.getComplaint(2L);
//        Assert.assertEquals(invoiceComplaint1, actual);
//    }

    @Test
    public void getComplaints() {
        Collection<InvoiceComplaint> actual = invoiceComplaintDao.getComplaints(invoice2);
        Assert.assertEquals(1, actual.size());
    }
}