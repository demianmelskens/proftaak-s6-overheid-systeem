package dao;

import domain.TimeInterval;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import util.DatabaseCleaner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class TimeIntervalDaoTest {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestGovernment_PU");
    private EntityManager em;
    private EntityTransaction tx;

    private TimeIntervalDaoImpl timeIntervalDao = new TimeIntervalDaoImpl();

    private TimeInterval timeInterval1 = new TimeInterval("time1", new Date(System.currentTimeMillis() - 84400000), new Date(System.currentTimeMillis()), 1.0);
    private TimeInterval timeInterval2 = new TimeInterval("time2", new Date(System.currentTimeMillis() - 84400000), new Date(System.currentTimeMillis()), 1.0);
    private TimeInterval timeInterval3 = new TimeInterval("time3", new Date(System.currentTimeMillis() - 84400000), new Date(System.currentTimeMillis()), 1.0);
    private TimeInterval timeInterval4 = new TimeInterval("time4", new Date(System.currentTimeMillis() - 84400000), new Date(System.currentTimeMillis()), 1.0);

    private List<TimeInterval> allTimes = new ArrayList<>();

    @Before
    public void setUp() {
        try {
            new DatabaseCleaner(emf.createEntityManager()).clean();
        } catch (SQLException ex) {
        }
        em = emf.createEntityManager();
        tx = em.getTransaction();
        timeIntervalDao.em = em;

        em.getTransaction().begin();
        timeIntervalDao.create(timeInterval1);
        timeIntervalDao.create(timeInterval2);
        timeIntervalDao.create(timeInterval3);
        em.getTransaction().commit();
        allTimes.add(timeInterval1);
        allTimes.add(timeInterval2);
        allTimes.add(timeInterval3);
    }

    @After
    public void breakDown() {
    }

    @Test
    public void testCount() {

        assertEquals(3, timeIntervalDao.count());
    }

    @Test
    public void testCreate() {
        assertEquals(3, timeIntervalDao.count());
        em.getTransaction().begin();
        timeIntervalDao.create(timeInterval4);
        em.getTransaction().commit();
        assertEquals(4, timeIntervalDao.count());
    }

    @Test
    public void testEdit() {
        em.getTransaction().begin();
        timeInterval1.setMultiplier(2.0);
        timeIntervalDao.edit(timeInterval1);
        em.getTransaction().commit();
        timeInterval2 = timeIntervalDao.findById(timeInterval1.getId());
        assertEquals(timeInterval1.getMultiplier(), timeInterval2.getMultiplier());
    }

    @Test
    public void testRemove() {
        assertEquals(3, timeIntervalDao.count());
        em.getTransaction().begin();
        timeIntervalDao.remove(timeInterval3);
        em.getTransaction().commit();
        assertEquals(2, timeIntervalDao.count());
    }

    @Test
    public void testFindAll() {
        List<TimeInterval> result = (List<TimeInterval>) timeIntervalDao.findAll();
        assertEquals(3, result.size());
        assertTrue(allTimes.contains(result.toArray()[0]));
        assertTrue(allTimes.contains(result.toArray()[1]));
        assertTrue(allTimes.contains(result.toArray()[2]));
    }

    @Test
    public void testFindById() {
        TimeInterval testM1 = timeIntervalDao.findById(timeInterval1.getId());
        TimeInterval testM2 = timeIntervalDao.findById(timeInterval2.getId());
        TimeInterval testM3 = timeIntervalDao.findById(timeInterval3.getId());
        assertEquals(testM1, timeInterval1);
        assertEquals(testM2, timeInterval2);
        assertEquals(testM3, timeInterval3);
    }

    @Test
    public void testFindByUsername() {
        TimeInterval testM1 = timeIntervalDao.findByName(timeInterval1.getName());
        TimeInterval testM2 = timeIntervalDao.findByName(timeInterval2.getName());
        TimeInterval testM3 = timeIntervalDao.findByName(timeInterval3.getName());
        assertEquals(testM1, timeInterval1);
        assertEquals(testM2, timeInterval2);
        assertEquals(testM3, timeInterval3);
    }
}

