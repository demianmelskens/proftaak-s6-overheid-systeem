package service;

import dao.template.TimeIntervalDao;
import domain.TimeInterval;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import static org.junit.Assert.*;

public class TimeIntervalServiceTest {

    private TimeIntervalService timeIntervalService;
    private TimeInterval t1;
    private TimeInterval t2;
    private TimeInterval t3;
    private List<TimeInterval> allTimeIntervals = new ArrayList<>();

    @Before
    public void setUp() {
        this.timeIntervalService = new TimeIntervalService();
        this.timeIntervalService.timeIntervalDao = Mockito.mock(TimeIntervalDao.class);

        t1 = new TimeInterval("time1", new Date(123, 2, 4), new Date(123, 2, 5), 2.0);
        t2 = new TimeInterval("time2", new Date(123, 3, 4), new Date(123, 3, 5), 2.0);
        t3 = new TimeInterval("time3", new Date(123, 2, 15), new Date(123, 2, 16), 2.0);

        allTimeIntervals.add(t1);
        allTimeIntervals.add(t2);

        when(timeIntervalService.timeIntervalDao.count()).thenReturn(allTimeIntervals.size());
        when(timeIntervalService.timeIntervalDao.create(any(TimeInterval.class))).thenReturn(t3);
        when(timeIntervalService.timeIntervalDao.edit(t3)).thenReturn(t3);
        when(timeIntervalService.timeIntervalDao.remove(any(TimeInterval.class))).thenReturn(t3);
        when(timeIntervalService.timeIntervalDao.findAll()).thenReturn(allTimeIntervals);
        when(timeIntervalService.timeIntervalDao.findById(anyLong())).thenReturn(t3);
        when(timeIntervalService.timeIntervalDao.findByName(anyString())).thenReturn(t3);
    }

    @Test
    public void getAllTimeIntervals() {
        List<TimeInterval> result = (List<TimeInterval>) timeIntervalService.getAllTimeIntervals();
        assertEquals(allTimeIntervals, result);
        assertEquals(2, result.size());
    }

    @Test
    public void findTimeIntervalById() {
        TimeInterval testT = timeIntervalService.findTimeIntervalById(t3.getId());
        assertEquals(t3, testT);
    }

    @Test
    public void findTimeIntervalByName() {
        TimeInterval testT = timeIntervalService.findTimeIntervalByName(t3.getName());
        assertEquals(t3, testT);
    }

    @Test
    public void addTimeInterval() {
        TimeInterval testT = timeIntervalService.addTimeInterval(t3);
        assertEquals(t3, testT);
    }

    @Test
    public void updateTimeInterval() {
        TimeInterval testT = timeIntervalService.updateTimeInterval(t3);
        assertEquals(t3, testT);
    }

    @Test
    public void deleteTimeInterval() {
        TimeInterval testT = timeIntervalService.deleteTimeInterval(t3);
        assertEquals(t3, testT);
    }
}
