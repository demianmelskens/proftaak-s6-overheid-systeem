package service;

import dao.template.RegionDao;
import domain.Region;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import util.Position;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import static org.junit.Assert.*;


public class RegionServiceTest {
    private RegionService regionService;
    private Region r1;
    private Region r2;
    private Region r3;
    private List<Region> allRegions = new ArrayList<>();

    @Before
    public void setUp() {
        this.regionService = new RegionService();
        this.regionService.regionDao = Mockito.mock(RegionDao.class);

        r1 = new Region("region1", 2.0, new Position(1f, 1f), new Position(2f, 2f));
        r2 = new Region("region2", 1.0, new Position(2f, 2f), new Position(3f, 3f));
        r3 = new Region("region3", 3.0, new Position(3f, 3f), new Position(4f, 4f));

        allRegions.add(r1);
        allRegions.add(r2);

        when(regionService.regionDao.count()).thenReturn(allRegions.size());
        when(regionService.regionDao.create(any())).thenReturn(r3);
        when(regionService.regionDao.edit(any())).thenReturn(r3);
        when(regionService.regionDao.remove(any())).thenReturn(r3);
        when(regionService.regionDao.findAll()).thenReturn(allRegions);
        when(regionService.regionDao.findById(anyLong())).thenReturn(r1);
        when(regionService.regionDao.findByName(anyString())).thenReturn(r2);
    }

    @Test
    public void getAllRegions() {
        List<Region> result = (List<Region>) regionService.getAllRegions();
        assertEquals(allRegions, result);
        assertEquals(2, result.size());
    }

    @Test
    public void findRegionById() {
        Region testR = regionService.findRegionById(r1.getId());
        assertEquals(r1, testR);
    }

    @Test
    public void findRegionByName() {
        Region testR = regionService.findRegionByName(r2.getName());
        assertEquals(r2, testR);
    }

    @Test
    public void addRegion() {
        Region testR = regionService.addRegion(r3);
        assertEquals(r3, testR);
    }

    @Test
    public void updateRegion() {
        Region testR = regionService.updateRegion(r3);
        assertEquals(r3, testR);
    }

    @Test
    public void deleteRegion() {
        Region testR = regionService.deleteRegion(r3);
        assertEquals(r3, testR);
    }
}
