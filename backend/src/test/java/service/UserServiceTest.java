package service;

import dao.template.UserDao;
import domain.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import util.Location;
import util.enumerations.UserType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import static org.junit.Assert.*;

public class UserServiceTest {
    private UserService userService;

    private Collection<User> allUsers = new ArrayList<>();

    private User user1;
    private User user2;
    private User user3;

    @Before
    public void setUp() throws Exception {
        this.userService = new UserService();
        this.userService.userDao = Mockito.mock(UserDao.class);
        this.userService.authUser = Mockito.mock(User.class);

        user1 = new User("user1", "testUser1","email@test.com", "password1", new Date(System.currentTimeMillis()), UserType.CUSTOMER, new Location("5612MA", "Rachelsmolen 1", "Eindhoven"));
        user2 = new User("user2", "testUser2","email@test.com", "password2", new Date(System.currentTimeMillis()), UserType.CUSTOMER, new Location("1056 ST", "Vespuccistraat 106", "Amsterdam"));
        user3 = new User("user3", "testUser3","email@test.com", "password3", new Date(System.currentTimeMillis()), UserType.ADMIN, new Location("4384 GM", "Johan Melchior Kemperstraat 69", "Vlissingen"));
        allUsers.add(user1);
        allUsers.add(user2);

        when(userService.userDao.count()).thenReturn(allUsers.size());
        when(userService.userDao.create(Mockito.any(User.class))).thenReturn(user3);
        when(userService.userDao.findAll()).thenReturn(allUsers);
        when(userService.userDao.findById(anyLong())).thenReturn(user1);
        when(userService.userDao.findByUsername(user1.getUsername())).thenReturn(user1);
        when(userService.userDao.findByUsername(user2.getUsername())).thenReturn(null);
        when(userService.userDao.findByUsername(user3.getUsername())).thenReturn(user1);
        when(userService.userDao.edit(user1)).thenReturn(user2);
        when(userService.userDao.remove(any(User.class))).thenReturn(user3);
    }

    @Test
    public void getAllVehicles() {
        List<User> expected = new ArrayList<>();
        expected.add(user1);
        expected.add(user2);
        List<User> result = (List<User>) userService.getAllUsers();
        assertEquals(allUsers, result);
        assertEquals(2, result.size());
    }

    @Test
    public void getVehicleCount() {
        int result = userService.countUsers();
        assertNotNull(result);
        assertEquals(2, result);
    }

    @Test
    public void updateVehicleTest() {
        user1 = userService.updateUser(user1);
        assertEquals(user2, user1);
    }

    @Test
    public void findVehicleById() {
        User testV1 = userService.findUserById(user1.getId());
        assertEquals(user1, testV1);
    }


    @Test
    public void findVehicleByOwnerId() {
        User testV1 = userService.findUserByUsername(user1.getUsername());
        assertEquals(user1, testV1);
    }

    @Test
    public void addVehicle() {
        User testV = userService.addUser(user3);
        assertEquals(user3, testV);
    }

    @Test
    public void deleteVehicle() {
        User testV = userService.deleteUser(user3);
        assertEquals(user3, testV);
    }

    @Test
    public void authenticate() {
        String token = userService.login(user1, new Date(System.currentTimeMillis()));
        assertNotNull(token);
    }

    @Test
    public void findAuthUser() {
        when(userService.userDao.findByUsername(any())).thenReturn(user1);
        User user = userService.findAuthUser();
        assertEquals(user1, user);
    }
}
