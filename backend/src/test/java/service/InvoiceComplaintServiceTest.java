package service;

import dao.template.InvoiceComplaintDao;
import domain.Invoice;
import domain.InvoiceComplaint;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import util.enumerations.InvoiceStatus;
import util.enumerations.PaymentStatus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceComplaintServiceTest {

    @Mock
    private InvoiceComplaintDao invoiceComplaintDao;

    @InjectMocks
    private InvoiceComplaintService invoiceComplaintService;

    private Invoice invoice1 = new Invoice(1L, 10.50, InvoiceStatus.SUCCESS, PaymentStatus.OPEN, new Date(System.currentTimeMillis()),  new Date(System.currentTimeMillis() + 28400000));
    private Invoice invoice2 = new Invoice(1L, 10.50, InvoiceStatus.WARNING, PaymentStatus.FULFILLED, new Date(System.currentTimeMillis()),  new Date(System.currentTimeMillis() + 28400000));

    private InvoiceComplaint invoiceComplaint1 = new InvoiceComplaint("is niet goed he", invoice1);
    private InvoiceComplaint invoiceComplaint2 = new InvoiceComplaint("jeroen doe dat nou niet!", invoice2);
    private Collection<InvoiceComplaint> complaints;

    @Before
    public void setUp() throws Exception {
        complaints = new ArrayList<InvoiceComplaint>();
        complaints.add(invoiceComplaint1);
        when(invoiceComplaintDao.getComplaint(1L)).thenReturn(invoiceComplaint1);
        when(invoiceComplaintDao.getComplaints(invoice1)).thenReturn(complaints);
    }

    @Test
    public void getComplaint() {
        InvoiceComplaint actual = invoiceComplaintService.getComplaint(1L);
        assertEquals(invoiceComplaint1, actual);
        verify(invoiceComplaintDao, times(1)).getComplaint(1L);
    }

    @Test
    public void getComplaints() {
        Collection<InvoiceComplaint> actual = invoiceComplaintService.getComplaints(invoice1);
        assertEquals(complaints, actual);
        verify(invoiceComplaintDao, times(1)).getComplaints(invoice1);
    }
}