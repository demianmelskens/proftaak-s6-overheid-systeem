package service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dao.template.InvoiceDao;
import dao.template.RegionDao;
import domain.Invoice;
import domain.Region;
import domain.TimeInterval;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.verification.VerificationMode;
import util.Position;
import util.enumerations.InvoiceStatus;
import util.enumerations.PaymentStatus;
import util.logic.InvoiceCalculator;
import util.logic.InvoiceDataRetriever;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class InvoiceServiceTest {
    private InvoiceService invoiceService;
    private RegionService regionService;
    private Invoice i1, i2, i3;
    private List<Invoice> allInvoices = new ArrayList<>(),
                          allInvoicesOfCustomer = new ArrayList<>(),
                          allInvoicesOpen = new ArrayList<>();
    private List<JsonObject> movements;
    private Set<JsonObject> vehicles;
    private Collection<Region> regions = new ArrayList<>();
    private Region r1, r2, r3, r4, r5, r6;
    private TimeInterval t1, t2, t3;

    @Before
    public void setUp() {
        this.invoiceService = new InvoiceService();
        this.regionService = new RegionService();
        this.regionService.regionDao = mock(RegionDao.class);
        this.invoiceService.invoiceDataRetriever = mock(InvoiceDataRetriever.class);
        this.invoiceService.invoiceCalculator = new InvoiceCalculator();
        this.invoiceService.invoiceCalculator.invoiceDataRetriever = mock(InvoiceDataRetriever.class);
        this.invoiceService.invoiceCalculator.regionService = this.regionService;
        this.invoiceService.invoiceDao = mock(InvoiceDao.class);

        // add data for testing
        addInvoiceData();
        addRegionData();
        addTimeintervalData();
        addVehicleData();
        addMovementData();

        // create mocks
        mockInvoiceService();
        mockInvoiceDateRetriever();
        mockRegionService();
    }

    @Test
    public void getAllRegions() {
        List<Invoice> result = (List<Invoice>) invoiceService.getAllInvoices();
        assertEquals(allInvoices, result);
        assertEquals(2, result.size());
    }

    @Test
    public void findRegionById() {
        Invoice result = invoiceService.findInvoiceById(i1.getId());
        assertEquals(i1, result);
    }

    @Test
    public void findRegionByCustomer() {
        Collection<Invoice> result = invoiceService.findInvoiceByCustomer(i2.getId());
        assertEquals(allInvoicesOfCustomer, result);
        assertEquals(2, result.size());
    }

    @Test
    public void findRegionByStatus() {
        Collection<Invoice> result = invoiceService.findInvoiceByStatus(i1.getPaymentStatus());
        assertEquals(allInvoicesOpen, result);
        assertEquals(1, result.size());
    }

    @Test
    public void addRegion() {
        Invoice result = invoiceService.addInvoice(i3);
        assertEquals(i3, result);
    }

    @Test
    public void updateRegion() {
        Invoice result = invoiceService.updateInvoice(i3);
        assertEquals(i3, result);
    }

    @Test
    public void deleteRegion() {
        Invoice result = invoiceService.deleteInvoice(i3);
        assertEquals(i3, result);
    }

    @Test
    public void filterQuery() {
        List<Invoice> result = (List<Invoice>) invoiceService.filterQuery(
                1L,
                2.0,
                4.0,
                "SUCCESS",
                "OPEN",
                "19-19-2018",
                "20-19-2018",
                "19-19-2018",
                "20-19-2018");
        verify(invoiceService.invoiceDao).filterQuery(anyString());
    }

    @Test
    public void filterQueryWithoutToFulfilledDate() {
        List<Invoice> result = (List<Invoice>) invoiceService.filterQuery(
                1L,
                2.0,
                4.0,
                "SUCCESS",
                "OPEN",
                "19-19-2018",
                "20-19-2018",
                "19-19-2018",
                null);
        verify(invoiceService.invoiceDao, times(1)).filterQuery(anyString());
    }

    @Test
    public void calculateTest(){
        invoiceService.calculateInvoice(1);
        verify(this.invoiceService.invoiceCalculator.invoiceDataRetriever, times(1)).getMovementsForVehicle(any());
    }

    @Test
    public void recalculateTest(){
        invoiceService.recalculateInvoice(1, i1);
        verify(this.invoiceService.invoiceCalculator.invoiceDataRetriever, times(1)).getMovementsForVehicle(any());
    }

    @Test
    public void calculateForeignersLicensePlates() {
        invoiceService.calculateForeignerInvoices();
        verify(this.invoiceService.invoiceDataRetriever, times(1)).getForeignersLicensePlates();
    }
    
    /**
     * adds invoices for testing
     */
    private void addInvoiceData(){
        try {
            i1 = new Invoice(1L, 3.0, InvoiceStatus.WARNING, PaymentStatus.OPEN, (new SimpleDateFormat("dd-MM-yyyy")).parse("18-04-2018"), (new SimpleDateFormat("dd-MM-yyyy")).parse("20-04-2018"));
            i2 = new Invoice(2L, 5.0, InvoiceStatus.SUCCESS, PaymentStatus.FULFILLED, (new SimpleDateFormat("dd-MM-yyyy")).parse("19-04-2018"), (new SimpleDateFormat("dd-MM-yyyy")).parse("19-04-2018"));
            i3 = new Invoice(3L, 4.0, InvoiceStatus.FAILED, PaymentStatus.CANCELLED, (new SimpleDateFormat("dd-MM-yyyy")).parse("20-04-2018"), (new SimpleDateFormat("dd-MM-yyyy")).parse("21-04-2018"));
        } catch (ParseException e){
            e.printStackTrace();
        }

        allInvoices.add(i1);
        allInvoices.add(i2);
        allInvoicesOfCustomer.add(i1);
        allInvoicesOfCustomer.add(i3);
        allInvoicesOpen.add(i1);
    }

    /**
     * adds regions for testing
     */
    private void addRegionData(){
        r1 = new Region("region1", 2.00, new Position((float)51.3048,(float)-10.7847), new Position((float)52.6925,(float)-8.08204));
        r2 = new Region("region2", 2.00, new Position((float)51.3048,(float)-8.08204), new Position((float)52.6925,(float)-5.3794));
        r3 = new Region("region3", 2.00, new Position((float)52.6925,(float)-10.7847), new Position((float)54.0802,(float)-8.08204));
        r4 = new Region("region4", 2.00, new Position((float)52.6925,(float)-8.08204), new Position((float)54.0802,(float)-5.3794));
        r5 = new Region("region5", 2.00, new Position((float)54.0802,(float)-10.7847), new Position((float)55.4679,(float)-8.08204));
        r6 = new Region("region6", 2.00, new Position((float)54.0802,(float)-8.08204), new Position((float)55.4679,(float)-5.3794));
    }

    /**
     * adds timeintervals for testing
     */
    private void addTimeintervalData(){
        t1 = new TimeInterval("morning", getDate("2018-05-08 00:00:01"), getDate("2018-05-08 08:00:00"),1.05);
        t2 = new TimeInterval("afternoon",getDate("2018-05-08 08:00:01"), getDate("2018-05-08 16:00:00"), 1.15);
        t3 = new TimeInterval("evening", getDate("2018-05-08 16:00:01"), getDate("2018-05-08 00:00:00"), 1.25);

        List<TimeInterval> intervals = new ArrayList<TimeInterval>();
        intervals.add(t1);
        intervals.add(t2);
        intervals.add(t3);

        r1.setTimeIntervals(intervals);
        r2.setTimeIntervals(intervals);
        r3.setTimeIntervals(intervals);
        r4.setTimeIntervals(intervals);
        r5.setTimeIntervals(intervals);
        r6.setTimeIntervals(intervals);

        regions.add(r1);
        regions.add(r2);
        regions.add(r3);
        regions.add(r4);
        regions.add(r5);
        regions.add(r6);
    }

    /**
     * adds movements for testing
     */
    private void addMovementData(){
        JsonParser jsonParser = new JsonParser();
        String json = "[{\"drivenDistance\":0.0242199007505551,\"id\":40,\"missed\":false,\"position\":{\"lat\":51.672724212403516,\"lon\":-9.453084380781307},\"serialNr\":\"dddb1a7162153478b944fcd136cf09ad86151a7fef89d77ab7ba159ca4bf7a85\",\"timestamp\":\"2018-05-18T11:34:24Z[UTC]\"},{\"drivenDistance\":0.087755956698386,\"id\":42,\"missed\":false,\"position\":{\"lat\":51.67282047282815,\"lon\":-9.452221611529954},\"serialNr\":\"dddb1a7162153478b944fcd136cf09ad86151a7fef89d77ab7ba159ca4bf7a85\",\"timestamp\":\"2018-05-18T11:34:29Z[UTC]\"}]";
        JsonArray array = jsonParser.parse(json).getAsJsonArray();
        List<JsonObject> movements = new ArrayList<JsonObject>();
        Iterator itr = array.iterator();
        while(itr.hasNext()) {
            movements.add(((JsonElement) itr.next()).getAsJsonObject());
        }
        this.movements = movements;
    }

    /**
     * adds vehicle for testing
     */
    private void addVehicleData(){
        JsonParser jsonParser = new JsonParser();
        String json = "[{\"brand\":\"BWM\",\"id\":1,\"licensePlate\":\"2854JD3\",\"links\":[{\"href\":\"http://192.168.24.21:8080/Register/api/vehicles/1/movements\",\"rel\":\"self\"}],\"model\":\"i8\",\"ownerId\":1,\"previouslyOwnedById\":[2],\"rateCategory\":\"CO2_0_1\",\"registeredCountry\":\"Netherlands\"}]";
        JsonArray array = jsonParser.parse(json).getAsJsonArray();
        Set<com.google.gson.JsonObject> vehicles = new HashSet<com.google.gson.JsonObject>();
        Iterator itr = array.iterator();
        while(itr.hasNext()) {
            vehicles.add(((JsonElement) itr.next()).getAsJsonObject());
        }
        this.vehicles = vehicles;
    }

    /**
     * mocks the invoice service
     */
    private void mockInvoiceService(){
        when(invoiceService.invoiceDao.create(any())).thenReturn(i3);
        when(invoiceService.invoiceDao.edit(any())).thenReturn(i3);
        when(invoiceService.invoiceDao.remove(any())).thenReturn(i3);
        when(invoiceService.invoiceDao.findAll()).thenReturn(allInvoices);
        when(invoiceService.invoiceDao.findById(anyLong())).thenReturn(i1);
        when(invoiceService.invoiceDao.findByCustomerId(anyLong())).thenReturn(allInvoicesOfCustomer);
        when(invoiceService.invoiceDao.findByStatus(PaymentStatus.OPEN)).thenReturn(allInvoicesOpen);
    }

    /**
     * mocks the region service
     */
    private void mockRegionService(){
        when(regionService.getAllRegions()).thenReturn(this.regions);
    }

    /**
     * mocks the invoice dataretriever
     */
    private void mockInvoiceDateRetriever(){
        when(invoiceService.invoiceDataRetriever.getVehicleDataForUser(1L)).thenReturn(this.vehicles);
        when(invoiceService.invoiceDataRetriever.getMovementsForVehicle("/vehicles/" + 1L + "/movements/month")).thenReturn(this.movements);
        when(invoiceService.invoiceDataRetriever.getRateCategoryPrice("CO2_0_1")).thenReturn(12.0);
        when(invoiceService.invoiceDataRetriever.getForeignersLicensePlates()).thenReturn(this.vehicles);
        when(invoiceService.invoiceDataRetriever.getTotalDistanceByVehicle(anyLong())).thenReturn(12.00);
    }

    /**
     * converts string into date
     * @param date
     * @return
     */
    private Date getDate(String date){
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
