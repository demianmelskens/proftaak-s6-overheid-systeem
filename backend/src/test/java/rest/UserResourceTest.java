package rest;

import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class UserResourceTest {

    @Test
    public void allUsers() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://192.168.24.20:8080/Overheid/api/users");
        Response response = target.request().get();

        assertEquals(200, response.getStatus());
    }
}
