package util;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Location implements Serializable{
    private String zipCode;
    private String address;
    private String city;

    public Location(String zipCode, String address, String city) {
        this.zipCode = zipCode;
        this.address = address;
        this.city = city;
    }

    public Location() {
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
