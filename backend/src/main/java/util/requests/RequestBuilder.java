package util.requests;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 * A builder that sends requests to the registration system
 */
public class RequestBuilder {
    private static Client client = ClientBuilder.newClient();
    private static String baseUrl = "http://192.168.24.21:8080/Register/api";

    /**
     * sets targets and does requests
     * @param url
     * @return Response
     */
    public static Response doRequest(String url){
        WebTarget target = client.target(baseUrl + url);
        return target.request().get();
    }

    /**
     * sets targets without baseurl and does requests
     * @param url
     * @return Response
     */
    public static Response doNativeRequest(String url){
        WebTarget target = client.target(url);
        return target.request().get();
    }
}