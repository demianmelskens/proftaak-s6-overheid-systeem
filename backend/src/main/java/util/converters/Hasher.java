package util.converters;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Startup
@Singleton
public class Hasher {

    /**
     * Converts the string into a hash.
     * @param stringToHash
     * @return String
     */
    public static String stringToHash(String stringToHash){
        //TODO: Review, implement salt into algorithm.
        String hashstring = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(stringToHash.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }

            hashstring = hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if(hashstring == null)
            return stringToHash;

        return hashstring;
    }
}
