package util;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Position implements Serializable{
    private Float lat;
    private Float lon;

    public Position(Float lat, Float lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public Position() {
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLon() {
        return lon;
    }

    public void setLon(Float lon) {
        this.lon = lon;
    }
}