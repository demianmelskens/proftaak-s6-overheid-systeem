package util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateFormatter {

    private static DateFormat finalFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private static DateFormat testFormat_1 = new SimpleDateFormat("dd-MM-yyyy");
    private static DateFormat testFormat_2 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    public static String formatDateToString(String date){
        if(isValidFormat(testFormat_1, date)){
            return finalFormat.format(parseFormat(testFormat_1, date));
        }else if(isValidFormat(testFormat_2, date)){
            return finalFormat.format(parseFormat(testFormat_2, date));
        }else{
            return date;
        }
    }

    public static String formatDateToString(Date date){
        return finalFormat.format(date);
    }

    private static Date parseFormat(DateFormat format, String date){
        try{
            return format.parse(date);
        }catch (ParseException ex){
            return null;
        }
    }

    private static boolean isValidFormat(DateFormat format, String value) {
        Date date = null;
        try {
            date = format.parse(value);
            if (!value.equals(format.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            date = null;
        }
        return date != null;
    }
}
