package util.generators;

import domain.User;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.security.Key;
import java.util.Date;

@Startup
@Singleton
public class AuthGenerator {

    private static Key key;

    /**
     *
     * @return
     */
    public static Key getKey() {
        if (key == null) {
            //returns a key with a random byte[64] and the SignatureAlgorithm
            key = MacProvider.generateKey();
        }
        return key;
    }

    /**
     * generates a new token
     * @param registeredCustomer to generete token for
     * @return String
     */
    public static String generateToken(User registeredCustomer, Date issuedAt) {
        Key key = getKey();
        return Jwts.builder()
                .setIssuedAt(issuedAt)
                .setSubject(registeredCustomer.getUsername())
                .compressWith(CompressionCodecs.DEFLATE)
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
    }
}