package util.enumerations;

public enum  UserType {
    ADMIN,
    CUSTOMER,
    EMPLOYEE
}
