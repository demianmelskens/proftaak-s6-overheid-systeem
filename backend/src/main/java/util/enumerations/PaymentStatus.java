package util.enumerations;

public enum PaymentStatus {
    OPEN,
    CANCELLED,
    FULFILLED
}
