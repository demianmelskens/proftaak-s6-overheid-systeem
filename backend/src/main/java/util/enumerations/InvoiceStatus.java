package util.enumerations;

public enum InvoiceStatus {
    SUCCESS,
    PENDING,
    WARNING,
    FAILED,
}
