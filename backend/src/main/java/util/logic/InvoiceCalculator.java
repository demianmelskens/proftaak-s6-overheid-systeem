package util.logic;

import com.google.gson.JsonObject;
import domain.Region;
import domain.TimeInterval;
import service.RegionService;
import util.Position;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

@Stateless
public class InvoiceCalculator {

    @Inject
    public RegionService regionService;

    @Inject
    public InvoiceDataRetriever invoiceDataRetriever;

    /**
     * starts the calculation process and creates the invoice object.
     * @param vehicles
     */
    public double calulate(Set<JsonObject> vehicles){
        double totalPrice = 0;
        for(JsonObject vehicle : vehicles){
            String category = vehicle.get("rateCategory").getAsString();
            double basePrice = invoiceDataRetriever.getRateCategoryPrice(category);
            List<JsonObject> movements = invoiceDataRetriever.getMovementsForVehicle("/vehicles/" + vehicle.get("id").getAsString() + "/movements/month");
            totalPrice += calculateTotalPrice(basePrice, movements);
        }
        return round(totalPrice, 2);
    }

    /**
     * calculates the the price per km for a movement
     * depending on region and timeinterval
     * @return double
     */
    private double calculatePricePerKM(JsonObject movement, Collection<Region> regions){
        double pricePerKM = 0;
        for (Region region : regions){
            JsonObject movementPosition = movement.get("position").getAsJsonObject();
            if(region.within(new Position(movementPosition.get("lat").getAsFloat(), movementPosition.get("lon").getAsFloat()))){
                for(TimeInterval timeInterval : region.getTimeIntervals()){
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                    Date movementTimestamp = sdf.parse(movement.get("timestamp").getAsString(), new ParsePosition(0));
                    if(movementTimestamp.after(timeInterval.getStartTime()) && movementTimestamp.before(timeInterval.getEndTime())){
                        pricePerKM = region.getBasePrice() * timeInterval.getMultiplier();
                        break;
                    }
                }
            }
        }
        return pricePerKM > 0 ? pricePerKM : 0.1;
    }

    /**
     * calculates the extra costs for a movement
     * by multiplying the distance with the price per km
     * @return double
     */
    private double calculateExtraCostsPerMovement(JsonObject movement, double distance, Collection<Region> regions){
        double kmPrice = calculatePricePerKM(movement, regions);
        return kmPrice * distance;
    }

    /**
     * calculates the total extra costs for all movements
     * by adding all the total movement cost on top of each other
     * @return double
     */
    private double calculateTotalExtraCosts(List<JsonObject> movements, Collection<Region> regions){
        double extraCosts = 0;
        double lastDistance = 0;
        for(JsonObject movement : movements){
            double drivenDistance = movement.get("drivenDistance").getAsDouble() - lastDistance;
            extraCosts += calculateExtraCostsPerMovement(movement, drivenDistance, regions);
            lastDistance = movement.get("drivenDistance").getAsDouble();
        }
        return extraCosts;
    }

    /**
     * calculates the total invoice price
     * by adding total movement costs to the basePrice
     * @return double
     */
    private double calculateTotalPrice(double basePrice, List<JsonObject> movements){
        double extraCosts = calculateTotalExtraCosts(movements, regionService.getAllRegions());
        return basePrice + extraCosts;
    }

    /**
     * rounds double value to the amount given
     * @param value
     * @param places
     * @return double
     */
    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
