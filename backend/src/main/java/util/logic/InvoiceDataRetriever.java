package util.logic;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.requests.RequestBuilder;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import java.util.*;

@Stateless
public class InvoiceDataRetriever {

    /**
     * used for retrieving all the data needed when generating invoices
     * @param userID
     * @return Set<JsonObject>
     */
    public Set<JsonObject> getVehicleDataForUser(long userID){
        Response response = RequestBuilder.doRequest("/vehicles/owner/" + userID);
        JsonParser jsonParser = new JsonParser();
        String json = response.readEntity(String.class);
        JsonArray array = jsonParser.parse(json).getAsJsonArray();
        Set<JsonObject> vehicles = new HashSet<JsonObject>();
        Iterator itr = array.iterator();
        while(itr.hasNext()) {
            vehicles.add(((JsonElement) itr.next()).getAsJsonObject());
        }
        return vehicles;
    }

    /**
     * user for retrieving all the movement data.
     * @param url
     * @return Set<JsonObject>
     */
    public List<JsonObject> getMovementsForVehicle(String url){
        Response response = RequestBuilder.doRequest(url);
        JsonParser jsonParser = new JsonParser();
        String json = response.readEntity(String.class);
        JsonArray array = jsonParser.parse(json).getAsJsonArray();
        List<JsonObject> movements = new ArrayList<JsonObject>();
        Iterator itr = array.iterator();
        while(itr.hasNext()) {
            movements.add(((JsonElement) itr.next()).getAsJsonObject());
        }
        return movements;
    }

    /**
     * user for retrieving all the movement data.
     * @param category
     * @return double
     */
    public double getRateCategoryPrice(String category){
        Response response = RequestBuilder.doRequest("/vehicles/ratecategory/" + category);
        return response.readEntity(double.class);
    }

    /**
     * Method that gets all the foreigners vehicles
     * @return
     */
    public Set<JsonObject> getForeignersLicensePlates() {
        Response response = RequestBuilder.doRequest("/vehicles/foreign/");
        Set<JsonObject> vehicles = new HashSet<>();
        if (response.hasEntity()) {
            JsonParser jsonParser = new JsonParser();
            String json = response.readEntity(String.class);
            JsonArray array = jsonParser.parse(json).getAsJsonArray();
            Iterator itr = array.iterator();
            while(itr.hasNext()) {
                vehicles.add(((JsonElement) itr.next()).getAsJsonObject());
            }
        }
        return vehicles;
    }

    /**
     * gets the total distance of the last movement by a vehicle.
     * @param vehicleID
     * @return
     */
    public double getTotalDistanceByVehicle(Long vehicleID) {
        try {
            Response response = RequestBuilder.doRequest("/vehicles/" + vehicleID + "/movements/latest/");
            if (response.hasEntity()) {
                if (response.getStatus() == 200) {
                    JsonParser jsonParser = new JsonParser();
                    String json = response.readEntity(String.class);
                    JsonObject object = jsonParser.parse(json).getAsJsonObject();
                    return object.get("drivenDistance").getAsDouble();
                }
            } else {
                return 0.00;
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return 0.00;
    }
}
