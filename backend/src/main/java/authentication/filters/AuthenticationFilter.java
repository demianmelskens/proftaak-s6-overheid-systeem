package authentication.filters;

import authentication.annotations.AuthenticatedUser;
import authentication.annotations.Secured;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import util.generators.AuthGenerator;

import javax.annotation.Priority;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.security.Key;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    private static final String AUTHENTICATION_SCHEME = "Bearer";
    private static final String REALM = "example";

    @Inject
    @AuthenticatedUser
    private Event<String> userAuthenticatedEvent;

    /**
     * checks if their is an authorization token in the request
     * if not abort with unauthorized response
     * else set the authorized user and go on.
     * @param containerRequestContext
     * @throws IOException
     */
    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        String authorizationHeader = containerRequestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        //Validate the authorization header
        if (authorizationHeader == null || authorizationHeader.isEmpty() || !authorizationHeader.toLowerCase().startsWith(AUTHENTICATION_SCHEME.toLowerCase() + " ")) {
            containerRequestContext.abortWith(getUnauthorizedResponse());
            return;
        }

        //extract the token from the authorization header
        String token = authorizationHeader.substring(AUTHENTICATION_SCHEME.length()).trim();

        //generates key if non existent else returns the key which already exists
        Key key = AuthGenerator.getKey();
        try {
            //retrieves the user name from the JWT token
            String userName = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().getSubject();
            userAuthenticatedEvent.fire(userName);
        }
        catch(IllegalArgumentException|SignatureException e) {
            containerRequestContext.abortWith(getUnauthorizedResponse());
        }
    }

    /**
     * returns an unauthorized response.
     * @return
     */
    private Response getUnauthorizedResponse() {
        return Response.status(Response.Status.UNAUTHORIZED)
                .header(HttpHeaders.WWW_AUTHENTICATE, AUTHENTICATION_SCHEME + " realm=\"" + REALM + "\"")
                .build();
    }
}