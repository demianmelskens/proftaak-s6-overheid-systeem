package authentication.producers;

import authentication.annotations.AuthenticatedUser;
import dao.template.UserDao;
import domain.User;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

@RequestScoped
public class AuthenticatedUserProducer {

    @Inject
    private UserDao userDao;

    @Produces
    @RequestScoped
    @AuthenticatedUser
    private User authenticatedCustomer;

    public void handleAuthenticationEvent(@Observes @AuthenticatedUser String userName) {
        authenticatedCustomer = userDao.findByUsername(userName);
    }
}