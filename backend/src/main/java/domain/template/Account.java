package domain.template;

import util.Location;

import java.util.Date;

public interface Account {
    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);

    String getUsername();

    void setUsername(String username);

    String getEmail();

    void setEmail(String email);

    String getPassword();

    void setPassword(String password);

    Date getExpireDate();

    void setExpireDate(Date date);

    Location getLocation();

    void setLocation(Location location);

}
