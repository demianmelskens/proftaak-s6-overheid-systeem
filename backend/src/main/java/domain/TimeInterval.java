package domain;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(name = "TimeInterval.allTimeInterval", query = "SELECT t FROM TimeInterval t"),
        @NamedQuery(name = "TimeInterval.findTimeIntervalById", query="SELECT t FROM TimeInterval t WHERE t.id = :id"),
        @NamedQuery(name = "TimeInterval.findTimeIntervalByName", query="SELECT t FROM TimeInterval t WHERE t.name = :name")
})
public class TimeInterval{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(nullable = false, unique = true, length = 64)
    private String name;

    @Column(nullable = false)
    private Double multiplier;

    @Column(name = "start_time", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    @Column(name = "end_time", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;

    @ManyToOne
    @JsonbTransient
    private Region region;

    @Transient
    private Collection<Link> links;

    public TimeInterval() {
        this.links = new ArrayList<Link>();
    }

    public TimeInterval(String name, Date startTime, Date endTime, Double multiplier) {
        this.links = new ArrayList<Link>();
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.multiplier = multiplier;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Double multiplier) {
        this.multiplier = multiplier;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Collection<Link> getLinks() {
        return links;
    }

    public void setLinks(Collection<Link> links) {
        this.links = links;
    }

    public void addLink(Link link){
        this.links.add(link);
    }

    public void resetLinks(){
        this.links.clear();
    }
}
