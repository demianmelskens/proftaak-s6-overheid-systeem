package domain;

import util.enumerations.InvoiceStatus;
import util.enumerations.PaymentStatus;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(name = "Invoice.count", query = "SELECT COUNT(i) FROM Invoice i"),
        @NamedQuery(name = "Invoice.allInvoices", query = "SELECT i FROM Invoice i"),
        @NamedQuery(name = "Invoice.allProblemInvoices", query = "SELECT i FROM Invoice i WHERE i.status = 'WARNING' OR i.status = 'FAILED'"),
        @NamedQuery(name = "Invoice.findInvoiceById", query="SELECT i FROM Invoice i WHERE i.id = :id"),
        @NamedQuery(name = "Invoice.findInvoiceByStatus", query="SELECT i FROM Invoice i WHERE i.paymentStatus = :paymentStatus"),
        @NamedQuery(name = "Invoice.findInvoiceByUser", query="SELECT i FROM Invoice i WHERE i.userId = :userId")
})
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "total_amount")
    private Double totalAmount;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private InvoiceStatus status;

    @Column(name = "payment_status")
    @Enumerated(EnumType.STRING)
    private PaymentStatus paymentStatus;

    @Column(name = "creation_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(name = "fulfilled_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fulfilledDate;

    @OneToMany(mappedBy = "invoice", cascade = CascadeType.PERSIST, orphanRemoval = true)
    @JsonbTransient
    private Collection<InvoiceComplaint> complaints;

    public Invoice() {
    }

    public Invoice(Long userId, Double totalAmount, InvoiceStatus invoiceStatus, PaymentStatus paymentStatus, Date creationDate, Date fulfilledDate) {
        this.userId = userId;
        this.totalAmount = totalAmount;
        this.status = invoiceStatus;
        this.paymentStatus = paymentStatus;
        this.creationDate = creationDate;
        this.fulfilledDate = fulfilledDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getFulfilledDate() {
        return fulfilledDate;
    }

    public void setFulfilledDate(Date fulfilledDate) {
        this.fulfilledDate = fulfilledDate;
    }

    public Collection<InvoiceComplaint> getComplaints() {
        return complaints;
    }

    public void setComplaints(Collection<InvoiceComplaint> complaints) {
        this.complaints = complaints;
    }

    public InvoiceStatus getStatus() {
        return status;
    }

    public void setStatus(InvoiceStatus status) {
        this.status = status;
    }
}
