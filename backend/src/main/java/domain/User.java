package domain;

import domain.template.Account;
import util.Location;
import util.converters.Hasher;
import util.enumerations.UserType;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(name = "User.allUsers", query = "SELECT u FROM User u"),
        @NamedQuery(name = "User.findUserById", query="SELECT u FROM User u WHERE u.id = :id"),
        @NamedQuery(name = "User.findUserByUsername", query = "SELECT u FROM User u WHERE u.username = :username")
})
public class User implements Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50)
    private String name;

    @Column(nullable = false, unique = true, length = 50)
    private String username;

    @Column(nullable = false)
    private String email;

    @Column(length = 256, nullable = false)
    private String password;

    @Column(name = "expire_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date expireDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private UserType type;

    @Embedded
    private Location location;

    public User() {
    }

    public User(String name, String username, String email, String password, Date expireDate, UserType type, Location location) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.expireDate = expireDate;
        this.type = type;
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonbTransient
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = Hasher.stringToHash(username + password);
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @JsonbTransient
    public UserType getType(){
        return this.type;
    }

    public void setType(UserType type){
        this.type = type;
    }


}
