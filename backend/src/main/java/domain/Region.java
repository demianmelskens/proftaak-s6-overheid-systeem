package domain;

import util.Position;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@NamedQueries({
        @NamedQuery(name = "Region.allRegions", query = "SELECT r FROM Region r"),
        @NamedQuery(name = "Region.findRegionById", query="SELECT r FROM Region r WHERE r.id = :id"),
        @NamedQuery(name = "Region.findRegionByName", query="SELECT r FROM Region r WHERE r.name = :name")
})
public class Region{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true, length = 64)
    private String name;

    @Column(nullable = false)
    private Double basePrice;

    @AttributeOverrides({
            @AttributeOverride(name="lat",
                    column=@Column(name="begin_lat")),
            @AttributeOverride(name="lon",
                    column=@Column(name="begin_lon"))
    })
    @Embedded
    private Position beginPoint;

    @AttributeOverrides({
            @AttributeOverride(name="lat",
                    column=@Column(name="end_lat")),
            @AttributeOverride(name="lon",
                    column=@Column(name="end_lon"))
    })
    @Embedded
    @Column(name = "end_point")
    private Position endPoint;

    @OneToMany(mappedBy = "region")
    private Collection<TimeInterval> timeIntervals;

    @Transient
    private Collection<Link> links;

    public Region() {
        this.links = new ArrayList<Link>();
    }

    public Region(String name, Double basePrice, Position beginPoint, Position endPoint) {
        this.links = new ArrayList<Link>();
        this.name = name;
        this.basePrice = basePrice;
        this.beginPoint = beginPoint;
        this.endPoint = endPoint;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getBasePrice() {
        return this.basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public Position getBeginPoint() {
        return beginPoint;
    }

    public void setBeginPoint(Position beginPoint) {
        this.beginPoint = beginPoint;
    }

    public Position getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(Position endPoint) {
        this.endPoint = endPoint;
    }

    @JsonbTransient
    public Collection<TimeInterval> getTimeIntervals() {
        return timeIntervals;
    }

    public void setTimeIntervals(Collection<TimeInterval> timeIntervals) {
        this.timeIntervals = timeIntervals;
    }

    public Collection<Link> getLinks() {
        return links;
    }

    public void setLinks(Collection<Link> links) {
        this.links = links;
    }

    public void addLink(Link link){
        this.links.add(link);
    }

    public void resetLinks(){
        this.links.clear();
    }

    public boolean within(Position pos){
        if(pos.getLat() >= this.beginPoint.getLat() &&
                pos.getLon() >= this.beginPoint.getLon() &&
                pos.getLat() <= this.endPoint.getLat() &&
                pos.getLon() <= this.endPoint.getLon()){
            return true;
        }

        return false;
    }
}
