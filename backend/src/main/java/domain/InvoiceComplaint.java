package domain;

import javax.persistence.*;

@Entity
@NamedQueries({
        @NamedQuery(name = "InvoiceComplaint.get", query = "SELECT i FROM InvoiceComplaint i WHERE i.id = :id"),
        @NamedQuery(name = "InvoiceComplaint.allInvoiceComplaints", query = "SELECT i FROM InvoiceComplaint i WHERE i.invoice = :invoice")
})
public class InvoiceComplaint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "complaint", nullable = false)
    private String complaint;

    @ManyToOne()
    @JoinColumn(name = "invoice")
    private Invoice invoice;

    public InvoiceComplaint() {
    }

    public InvoiceComplaint(String complaint, Invoice invoice) {
        this.complaint = complaint;
        this.invoice = invoice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComplaint() {
        return complaint;
    }

    public void setComplaint(String complaint) {
        this.complaint = complaint;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
