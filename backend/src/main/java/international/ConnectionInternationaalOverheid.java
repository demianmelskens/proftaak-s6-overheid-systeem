package international;

import com.google.gson.Gson;
import com.s61.integration.connector.InternationalConnector;
import com.s61.integration.model.Countries;
import com.s61.integration.model.InternationalInvoice;
import domain.Invoice;
import kotlin.Unit;
import service.InvoiceService;
import util.enumerations.InvoiceStatus;
import util.enumerations.PaymentStatus;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.Random;

@Singleton
@Startup
public class ConnectionInternationaalOverheid {

    private InternationalConnector connector = null;
    private static Client client = ClientBuilder.newClient();
    private static String registerBaseURL = "http://192.168.24.21:8080/Register/api";

    private static ConnectionInternationaalOverheid instance;

    public static ConnectionInternationaalOverheid getInstance() {
        return instance;
    }

    @Inject
    InvoiceService invoiceService;

    public ConnectionInternationaalOverheid() {
    }

    @PostConstruct
    public void init() {
        connector = new InternationalConnector("rabbitmq", "rabbitmq", "vhost", "18.197.180.1");

        connector.subscribeToQueue(Countries.IRELAND, InternationalInvoice.class, ((String message) -> {
            receiveInvoiceMessage(message);
            return null;
        }));

        try {
            sendInvoice("Test2", 12.00, 23.00, new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        instance = this;
    }

    /**
     * if de server crashes it will correctly close the connection.
     */
    @PreDestroy
    public void destroy() {
        connector.close();
    }

    /**
     * Method listener to the invoice subscribe function.
     * @param message GSON message with InternationalInvoice.
     */
    public void receiveInvoiceMessage(String message) {
        Gson gson = new Gson();
        try {
            InternationalInvoice interInvoice = gson.fromJson(message, InternationalInvoice.class);

            WebTarget target = client.target(registerBaseURL + "/vehicles/vehicle/" + interInvoice.getLicencePlate() + "/owner");
            Response response = target.request().accept(MediaType.APPLICATION_JSON).get();
            if (response.hasEntity()) {
                String userID = response.readEntity(String.class);
                if (userID != null  && !userID.equals("-1")) {
                    try {
                        Long userIDL = Long.parseLong(userID);
                        Invoice invoice = new Invoice(userIDL, interInvoice.getPrice(), InvoiceStatus.PENDING, PaymentStatus.OPEN, interInvoice.getCreatedDate(), interInvoice.getDueByDate());
                        invoiceService.addInvoice(invoice);
                        System.out.println("invoice " + interInvoice);
                    } catch (NumberFormatException e) {
                        return;
                    }
                    return;
                }
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }

    }

    /**
     * Method for sending a invoice to another country
     *
     * @param licencePlate of the car
     * @param price        total price
     * @param distance     total car drives
     * @param dueByDate
     * @param createdDate
     * @throws Exception if it fails.
     */
    public void sendInvoice(String licencePlate, double price, double distance, Date dueByDate, Date createdDate) throws Exception {
        InternationalInvoice invoice = new InternationalInvoice(licencePlate, price, distance, dueByDate, createdDate);
        connector.publishInvoice(invoice);
    }

    /**
     * Gives enumeration back of country
     *
     * @param country
     * @return
     * @throws Exception
     */
    private Countries getDestinationCountry(String country) throws Exception {
        switch (country) {
            case "AUSTRIA": {
                return Countries.AUSTRIA;
            }
            case "NOORWEGEN": {
                return Countries.NORWAY;
            }
            case "LUXEMBOURG": {
                return Countries.LUXEMBOURG;
            }
            case "IRELAND": {
                return Countries.IRELAND;
            }
        }
        throw new Exception("COUNTRY WASN'T FOUND, YOU TRIED: " + country);
    }

    /**
     * Get Random country except for IRELAND.
     * @return
     * @throws Exception
     */
    private Countries getRandomCountry() throws Exception {
        Random random = new Random();
        switch (random.nextInt(3) + 1) {
            case 1: {
                return Countries.AUSTRIA;
            }
            case 2: {
                return Countries.NORWAY;
            }
            case 3: {
                return Countries.LUXEMBOURG;
            }
            case 4: {
                return Countries.IRELAND;
            }
        }
        return null;
    }
}
