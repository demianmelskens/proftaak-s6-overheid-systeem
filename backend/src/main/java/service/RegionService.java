package service;

import dao.template.RegionDao;
import domain.Region;
import util.enumerations.PaymentStatus;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collection;

@Stateless
public class RegionService {
    @Inject
    RegionDao regionDao;

    public Collection<Region> getAllRegions(){
        return regionDao.findAll();
    }

    public Region findRegionById(Long id){
        return regionDao.findById(id);
    }

    public Region findRegionByName(String name){
        return regionDao.findByName(name);
    }

    public Region addRegion(Region region){
        return regionDao.create(region);
    }

    public Region updateRegion(Region region){
        return regionDao.edit(region);
    }

    public Region deleteRegion(Region region){
        return regionDao.remove(region);
    }
}
