package service;

import com.google.gson.JsonObject;
import dao.template.InvoiceDao;
import domain.Invoice;
import international.ConnectionInternationaalOverheid;
import util.DateFormatter;
import util.enumerations.InvoiceStatus;
import util.enumerations.PaymentStatus;
import util.logic.InvoiceCalculator;
import util.logic.InvoiceDataRetriever;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collection;
import java.util.Date;
import java.util.Set;


@Stateless
public class InvoiceService {

    @Inject
    InvoiceDao invoiceDao;

    @Inject
    InvoiceCalculator invoiceCalculator;

    @Inject
    InvoiceDataRetriever invoiceDataRetriever;

    /**
     * retrieve all invoices
     * @return Collection<Invoice>
     */
    public Collection<Invoice> getAllInvoices(){
        return invoiceDao.findAll();
    }

    /**
     * find an invoice by id
     * @param id
     * @return Invoice
     */
    public Invoice findInvoiceById(Long id){
        return invoiceDao.findById(id);
    }

    /**
     * returns a invoice based on id year and date.
     * @param userId
     * @param month
     * @param year
     * @return Invoice
     */
    public Invoice findByUserIDAndDate(Long userId, int month, int year){
        return invoiceDao.findByUserIDAndDate("SELECT * FROM INVOICE i where i.user_id = " + userId + " and month(i.creation_date) = " + month + " and year(i.creation_date) = " + year);
    }

    /**
     * finds invoices by status
     * @param paymentStatus
     * @return Collection<Invoice>
     */
    public Collection<Invoice> findInvoiceByStatus(PaymentStatus paymentStatus){
        return invoiceDao.findByStatus(paymentStatus);
    }

    /**
     * finds invoices by userid
     * @param userId
     * @return Collection<Invoice>
     */
    public Collection<Invoice> findInvoiceByCustomer(Long userId){
        return invoiceDao.findByCustomerId(userId);
    }

    /**
     * adds an invoice
     * @param invoice
     * @return Invoice
     */
    public Invoice addInvoice(Invoice invoice){
        return invoiceDao.create(invoice);
    }

    /**
     * updates invoice
     * @param invoice
     * @return Invoice
     */
    public Invoice updateInvoice(Invoice invoice){
        return invoiceDao.edit(invoice);
    }

    /**
     * deletes invoice
     * @param invoice
     * @return Invoice
     */
    public Invoice deleteInvoice(Invoice invoice){
        return invoiceDao.remove(invoice);
    }

    /**
     * Build a dynamic query to filter the output
     * @param userId
     * @param minAmount
     * @param maxAmount
     * @param invoiceStatus
     * @param paymentStatus
     * @param fromCreationDate
     * @param toCreationDate
     * @param fromFulfilledDate
     * @param toFulfilledDate
     * @return Collection<Invoice>
     */
    public Collection<Invoice> filterQuery(Long userId, Double minAmount, Double maxAmount, String invoiceStatus, String paymentStatus,
                                           String fromCreationDate, String toCreationDate, String fromFulfilledDate, String toFulfilledDate) {
        String query = "SELECT i.* FROM INVOICE i ";
        String filter = "WHERE ";

        if (userId != null) {
            filter += "user_id = " + userId + " AND ";
        } if (minAmount != null) {
            filter += "total_amount > " + minAmount + " AND ";
        } if (maxAmount != null) {
            filter += "total_amount < " + maxAmount + " AND ";
        } if (invoiceStatus != null) {
            filter += "status = \"" + invoiceStatus + "\" AND ";
        } if (paymentStatus != null) {
            filter += "payment_status = \"" + paymentStatus + "\" AND ";
        } if (fromCreationDate != null) {
            filter += "creation_date > \"" + DateFormatter.formatDateToString(fromCreationDate) + "\" AND ";
        } if (toCreationDate != null) {
            filter += "creation_date < \"" + DateFormatter.formatDateToString(toCreationDate) + "\" AND ";
        } if (fromFulfilledDate != null) {
            filter += "fulfilled_date > \"" + DateFormatter.formatDateToString(fromFulfilledDate) + "\" AND ";
        } if (toFulfilledDate != null) {
            filter += "fulfilled_date < \"" + DateFormatter.formatDateToString(toFulfilledDate) + "\"";
        }

        if (filter.endsWith(" AND ")) {
            filter = filter.substring(0, filter.lastIndexOf("AND"));
        }
        if (!filter.equals("WHERE ")) {
            query += filter;
        }

        return invoiceDao.filterQuery(query);
    }

    private double getTotalPriceByVehicles(Set<JsonObject> vehiclesForUser) {
        return invoiceCalculator.calulate(vehiclesForUser);
    }

    /**
     * asynchronous method that calculates the invoices
     * @param userID
     */
    @Asynchronous
    public void calculateInvoice(long userID){
        Set<JsonObject> vehiclesForUser = invoiceDataRetriever.getVehicleDataForUser(userID);
        double totalPrice = getTotalPriceByVehicles(vehiclesForUser);

        if(totalPrice > 0) {
            addInvoice(new Invoice(userID, totalPrice, InvoiceStatus.PENDING, PaymentStatus.OPEN, new Date(System.currentTimeMillis()), null));
        }
    }

    /**
     * asynchronous method that recalculates the invoices
     * @param userID
     */
    @Asynchronous
    public void recalculateInvoice(long userID, Invoice original){
        Set<JsonObject> vehiclesForUser = invoiceDataRetriever.getVehicleDataForUser(userID);
        double totalPrice = invoiceCalculator.calulate(vehiclesForUser);
        original.setTotalAmount(totalPrice);
        original.setPaymentStatus(PaymentStatus.OPEN);
        original.setCreationDate(new Date(System.currentTimeMillis()));
        updateInvoice(original);
    }

    /**
     * asynchronous method that calculates al foreign vehicles.
     */
    @Asynchronous
    public void calculateForeignerInvoices() {
        Set<JsonObject> foreignVehicles = invoiceDataRetriever.getForeignersLicensePlates();
        double totalPrice = getTotalPriceByVehicles(foreignVehicles);
        for (JsonObject vehicles : foreignVehicles) {
            try {
                if (totalPrice != 0.00) {
                    Double drivenDistance =  invoiceDataRetriever.getTotalDistanceByVehicle(vehicles.get("id").getAsLong());
                    ConnectionInternationaalOverheid.getInstance().sendInvoice(
                            vehicles.get("licensePlate").getAsString(),
                            totalPrice,
                            drivenDistance,
                            new Date(System.currentTimeMillis()),
                            null);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
