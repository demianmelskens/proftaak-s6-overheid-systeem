package service;

import dao.template.InvoiceComplaintDao;
import domain.Invoice;
import domain.InvoiceComplaint;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collection;

@Stateless
public class InvoiceComplaintService {

    @Inject
    private InvoiceComplaintDao invoiceComplaintDao;

    /**
     * returns the complaint with id
     * @param id
     * @return InvoiceComplaint
     */
    public InvoiceComplaint getComplaint(long id) {
        return invoiceComplaintDao.getComplaint(id);
    }

    /**
     * returns list of complaints for the invoice
     * @param invoice
     * @return Collection<InvoiceComplaint>
     */
    public Collection<InvoiceComplaint> getComplaints(Invoice invoice) {
        return invoiceComplaintDao.getComplaints(invoice);
    }
}
