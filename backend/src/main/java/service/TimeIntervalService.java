package service;


import dao.template.TimeIntervalDao;
import domain.TimeInterval;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collection;

@Stateless
public class TimeIntervalService {
    @Inject
    TimeIntervalDao timeIntervalDao;

    public Collection<TimeInterval> getAllTimeIntervals(){
        return timeIntervalDao.findAll();
    }

    public TimeInterval findTimeIntervalById(Long id){
        return timeIntervalDao.findById(id);
    }

    public TimeInterval findTimeIntervalByName(String name){
        return timeIntervalDao.findByName(name);
    }

    public TimeInterval addTimeInterval(TimeInterval timeInterval){
        return timeIntervalDao.create(timeInterval);
    }

    public TimeInterval updateTimeInterval(TimeInterval timeInterval){
        return timeIntervalDao.edit(timeInterval);
    }

    public TimeInterval deleteTimeInterval(TimeInterval timeInterval){
        return timeIntervalDao.remove(timeInterval);
    }
}
