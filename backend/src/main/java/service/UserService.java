package service;

import authentication.annotations.AuthenticatedUser;
import dao.template.UserDao;
import domain.User;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import util.converters.Hasher;
import util.generators.AuthGenerator;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.security.Key;
import java.util.Collection;
import java.util.Date;

@Stateless
public class UserService {

    @Inject
    @AuthenticatedUser
    User authUser;

    @Inject
    UserDao userDao;

    public Collection<User> getAllUsers(){
        return userDao.findAll();
    }

    public int countUsers(){
        return userDao.count();
    }

    public User findUserById(Long id){
        return userDao.findById(id);
    }

    public User findUserByUsername(String username){
        return userDao.findByUsername(username);
    }

    public User addUser(User user){
        return userDao.create(user);
    }

    public User updateUser(User user){
        return userDao.edit(user);
    }

    public User deleteUser(User user){
        return userDao.remove(user);
    }

    /**
     * Authenticate registered customer
     *
     * @param user
     * @param issuedAt
     * @return String
     */
    public String login(User user, Date issuedAt) {
        return AuthGenerator.generateToken(user, issuedAt);
    }

    public User findAuthUser() {
        return userDao.findById(authUser.getId());
    }

    public String generateToken(User user) {
        Date now = new Date(System.currentTimeMillis());

        Key key = AuthGenerator.getKey();
        return Jwts.builder()
                .setIssuedAt(now)
                .setSubject(user.getUsername())
                .compressWith(CompressionCodecs.DEFLATE)
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
    }
}
