package rest;

import authentication.annotations.Secured;
import domain.User;
import domain.template.Account;
import service.UserService;
import util.converters.Hasher;
import util.enumerations.UserType;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.persistence.NoResultException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Collection;
import java.util.Date;

@Path("users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class UserResource {

    @Inject
    UserService userService;

    /**
     * return alls the users
     * @return Collection<User>
     */
    @GET
    @Secured
    public Collection<User> allUsers() {
        return userService.getAllUsers();
    }

    /**
     * return user from id
     * @param id
     * @return User
     */
    @GET
    @Path("{id}")
    @Secured
    public User getUserById(@PathParam("id") Long id) {
        return userService.findUserById(id);
    }

    /**
     * return user by username.
     * @param username
     * @return User
     */
    @GET
    @Path("username/{username}")
    @Secured
    public User getUserByUsername(@PathParam("username") String username) {
        return userService.findUserByUsername(username);
    }

    /**
     * returns the authenticated user.
     * @return Response
     */
    @GET
    @Path("user")
    @Secured
    public Response getAuthenticatedUser() {
        User user = userService.findAuthUser();
        return Response.ok(user).build();
    }

    /**
     * logs in the customer and return a JWT Token for further authentication
     * @param username username of user
     * @param password password of user
     * @return Response with RegisteredCustomer/null
     */
    @POST
    @Path("/login")
    public Response login(@HeaderParam("username") String username, @HeaderParam("password") String password){
        String pass = Hasher.stringToHash(username + password);
        User user = userService.findUserByUsername(username);
        if (user.getPassword().equals(pass)) {
            String token = userService.login(user, new Date(System.currentTimeMillis()));
            JsonObject jsonToken = Json.createObjectBuilder().add("token", token).build();
            return Response.ok(jsonToken, MediaType.APPLICATION_JSON).build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    /**
     * create a new user
     * @param user
     * @param uriInfo
     * @return Response
     */
    @POST
    @Secured
    public Response addUser(User user, @Context UriInfo uriInfo) {
        try {
            User u = userService.addUser(user);
            return Response.ok(u).build();
        } catch (NoResultException noResultException) {
            return Response.noContent().build();
        } catch (Exception exception) {
            return Response.serverError().build();
        }
    }

    /**
     * update existing user.
     * @param user
     * @param uriInfo
     * @return Response
     */
    @PUT
    @Secured
    public Response updateUser(User user, @Context UriInfo uriInfo) {
        try {
            User u = userService.updateUser(user);
            return Response.ok(u).build();
        } catch (NoResultException noResultException) {
            return Response.noContent().build();
        } catch (Exception exception) {
            return Response.serverError().build();
        }
    }

    /**
     * deletes existing user.
     * @param id
     * @param uriInfo
     * @return Response
     */
    @DELETE
    @Path("{id}")
    @Secured
    public Response deleteUser(@PathParam("id") Long id, @Context UriInfo uriInfo) {
        try {
            User user = userService.findUserById(id);
            userService.deleteUser(user);
            return Response.ok(user).build();
        } catch (NoResultException noResultException) {
            return Response.noContent().build();
        } catch (Exception exception) {
            return Response.serverError().build();
        }
    }

        /**
     * method login general consumer.
     * if the user "Consumer" isn't in the database it will register it self.
     * then it will login the consumer.
     * @return token for the consumer
     */
    @GET
    @Path("consumerLogin")
    public Response loginGeneralConsumer() {
        User user = new User(
                "Consumer",
                "Consumer",
                "Consomuer@consumer.com",
                "4fba3e295d1f07b5fa3b94b0e830dee71591c7cc7f5ffedf9fd3edf47dcb2b50", // Consumer
                new Date(),
                UserType.CUSTOMER,
                null);
        User consumer = null;
        try {
            consumer = getUserByUsername("Consumer");
        }
        catch (Exception e) {
            System.out.println(e);
        }
        if (consumer == null) {
            consumer = userService.addUser(user);
        }
        return login("Consumer", "Consumer");
    }
}
