package rest;

import authentication.annotations.Secured;
import domain.Link;
import domain.Region;
import domain.TimeInterval;
import service.RegionService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Collection;

@Path("regions")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class RegionResource {
    @Inject
    RegionService regionService;

    @Context
    UriInfo uriInfo;

    /**
     * retrieves all the regions and sets the hateaos links for them.
     * @return
     */
    @GET
    @Secured
    public Collection<Region> allRegions() {
        Collection<Region> regions = regionService.getAllRegions();
        for (Region r : regions){
            this.generateRegionLinks(r);
        }
        return regions;
    }

    /**
     * gets the region based on id.
     * generates the hateaos links for self and intervals.
     * @param id
     * @return
     */
    @GET
    @Path("{id}")
    @Secured
    public Region getRegionById(@PathParam("id") Long id) {
        Region region = regionService.findRegionById(id);
        this.generateRegionLinks(region);
        return region;
    }

    /**
     * gets the region based on the name
     * and generates the hateaos links for self and intervals.
     * @param name
     * @return
     */
    @GET
    @Path("name/{name}")
    @Secured
    public Region getRegionByName(@PathParam("name") String name) {
        Region region = regionService.findRegionByName(name);
        this.generateRegionLinks(region);
        return region;
    }

    /**
     * returns all the timeinterval for a specific region.
     * @param id
     * @return
     */
    @GET
    @Path("{id}/timeintervals")
    @Secured
    public Response getTimeIntervalsForRegion(@PathParam("id") long id){
        Collection<TimeInterval> intervals = regionService.findRegionById(id).getTimeIntervals();
        for(TimeInterval t : intervals){
            this.generateTimeIntervalLinks(t);
        }
        return Response.ok(intervals).build();
    }

    /**
     * adds a new region to database.
     * @param region
     * @return
     */
    @POST
    @Secured
    public Response addRegion(Region region) {
        try {
            Region r = regionService.addRegion(region);
            this.generateRegionLinks(region);
            return Response.ok(r).build();
        } catch (NoResultException noResultException) {
            return Response.noContent().build();
        } catch (Exception exception) {
            return Response.serverError().build();
        }
    }

    /**
     * changes the region.
     * @param region
     * @return
     */
    @PUT
    @Secured
    public Response updateRegion(Region region) {
        try {
            Region r = regionService.updateRegion(region);
            this.generateRegionLinks(region);
            return Response.ok(r).build();
        } catch (NoResultException noResultException) {
            return Response.noContent().build();
        } catch (Exception exception) {
            return Response.serverError().build();
        }
    }

    /**
     * deletes the regions based on the id
     * @param id
     * @param uriInfo
     * @return
     */
    @DELETE
    @Path("{id}")
    @Secured
    public Response deleteRegion(@PathParam("id") Long id, @Context UriInfo uriInfo) {
        try {
            Region region = regionService.findRegionById(id);
            regionService.deleteRegion(region);
            return Response.ok(region).build();
        } catch (NoResultException noResultException) {
            return Response.noContent().build();
        } catch (Exception exception) {
            return Response.serverError().build();
        }
    }

    /**
     * generates the hateoaslinks
     * @param region
     */
    private void generateRegionLinks(Region region){
        region.resetLinks();
        this.getUriSelfRegion(region);
        this.getUriIntervals(region);
    }

    /**
     * generates the hateoaslinks
     * @param timeInterval
     */
    private void generateTimeIntervalLinks(TimeInterval timeInterval){
        timeInterval.resetLinks();
        this.getUriSelfTimeInterval(timeInterval);
        this.getUriRegion(timeInterval);
    }

    /**
     * generates and adds the link for itself
     * @param region
     */
    private void getUriSelfRegion(Region region){
        String uri = uriInfo.getBaseUriBuilder()
                .path(RegionResource.class)
                .path(region.getId().toString())
                .build()
                .toString();
        region.addLink(new Link(uri, "self"));
    }

    /**
     * generates and adds the link for itself
     * @param timeInterval
     */
    private void getUriSelfTimeInterval(TimeInterval timeInterval){
        String uri = uriInfo.getBaseUriBuilder()
                .path(TimeIntervalResource.class)
                .path(timeInterval.getId().toString())
                .build()
                .toString();
        timeInterval.addLink(new Link(uri, "self"));
    }

    /**
     * generates and adds the link for timeintervals
     * @param region
     */
    private void getUriIntervals(Region region){
        String uri = uriInfo.getBaseUriBuilder()
                .path(RegionResource.class)
                .path(RegionResource.class, "getTimeIntervalsForRegion")
                .resolveTemplate("id", region.getId())
                .build()
                .toString();
        region.addLink(new Link(uri, "intervals"));
    }

    /**
     * generates and adds the link for region
     * @param timeInterval
     */
    private void getUriRegion(TimeInterval timeInterval){
        String uri = uriInfo.getBaseUriBuilder()
                .path(RegionResource.class)
                .path(timeInterval.getRegion().getId().toString())
                .build()
                .toString();
        timeInterval.addLink(new Link(uri, "intervals"));
    }
}
