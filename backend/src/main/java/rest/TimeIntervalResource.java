package rest;

import authentication.annotations.Secured;
import domain.TimeInterval;
import service.TimeIntervalService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Collection;

@Path("intervals")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class TimeIntervalResource {
    
    @Inject
    TimeIntervalService timeIntervalService;

    /**
     * returns all timeintervals
     * @return Collection<TimeInteval>
     */
    @GET
    public Collection<TimeInterval> allTimeIntervals() {
        return timeIntervalService.getAllTimeIntervals();
    }

    /**
     * returns a timeinterval based on id
     * @param id
     * @return TimeInterval
     */
    @GET
    @Path("{id}")
    public TimeInterval getTimeIntervalById(@PathParam("id") Long id) {
        return timeIntervalService.findTimeIntervalById(id);
    }

    /**
     * returns a timeinterval based on name
     * @param name
     * @return TimeInterval
     */
    @GET
    @Path("name/{name}")
    public TimeInterval getTimeIntervalByName(@PathParam("name") String name) {
        return timeIntervalService.findTimeIntervalByName(name);
    }

    /**
     * adds a new timeinterval to database
     * @param timeInterval
     * @param uriInfo
     * @return
     */
    @POST
    @Secured
    public Response addTimeInterval(TimeInterval timeInterval, @Context UriInfo uriInfo) {
        try {
            TimeInterval r = timeIntervalService.addTimeInterval(timeInterval);
            return Response.ok(r).build();
        } catch (NoResultException noResultException) {
            return Response.noContent().build();
        } catch (Exception exception) {
            return Response.serverError().build();
        }
    }

    /**
     * changes a timeinteval
     * @param timeInterval
     * @param uriInfo
     * @return
     */
    @PUT
    @Secured
    public Response updateTimeInterval(TimeInterval timeInterval, @Context UriInfo uriInfo) {
        try {
            TimeInterval r = timeIntervalService.updateTimeInterval(timeInterval);
            return Response.ok(r).build();
        } catch (NoResultException noResultException) {
            return Response.noContent().build();
        } catch (Exception exception) {
            return Response.serverError().build();
        }
    }

    /**
     * deletes timeinterval
     * @param id
     * @param uriInfo
     * @return
     */
    @DELETE
    @Path("{id}")
    @Secured
    public Response deleteTimeInterval(@PathParam("id") Long id, @Context UriInfo uriInfo) {
        try {
            TimeInterval timeInterval = timeIntervalService.findTimeIntervalById(id);
            timeIntervalService.deleteTimeInterval(timeInterval);
            return Response.ok(timeInterval).build();
        } catch (NoResultException noResultException) {
            return Response.noContent().build();
        } catch (Exception exception) {
            return Response.serverError().build();
        }
    }
}