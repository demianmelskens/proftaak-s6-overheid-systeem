package rest;

import authentication.annotations.Secured;
import domain.Invoice;
import service.InvoiceComplaintService;
import service.InvoiceService;
import util.enumerations.PaymentStatus;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.lang.annotation.Repeatable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

@Path("invoices")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class InvoiceResource {

    @Inject
    InvoiceService invoiceService;

    @Inject
    InvoiceComplaintService invoiceComplaintService;

    @GET
    @Secured
    public Collection<Invoice> allInvoices() {
        return invoiceService.getAllInvoices();
    }

    @GET
    @Secured
    @Path("query")
    public Collection<Invoice> filterInvoices(
            @QueryParam("userId") Long userId,
            @QueryParam("min_amount") Double minAmount,
            @QueryParam("max_amount") Double maxAmount,
            @QueryParam("invoice_status") String invoiceStatus,
            @QueryParam("payment_status") String paymentStatus,
            @QueryParam("from_creation_date") String fromCreationDate,
            @QueryParam("to_creation_date") String toCreationDate,
            @QueryParam("from_fulfilled_date") String fromFulfilledDate,
            @QueryParam("to_fulfilled_date") String toFulfilledDate) {
        return invoiceService.filterQuery(userId, minAmount, maxAmount, invoiceStatus, paymentStatus, fromCreationDate, toCreationDate, fromFulfilledDate, toFulfilledDate);
    }

    @GET
    @Secured
    @Path("{id}")
    public Invoice getInvoiceById(@PathParam("id") Long id) {
        return invoiceService.findInvoiceById(id);
    }

    @GET
    @Secured
    @Path("status/{status}")
    public Collection<Invoice> getInvoiceByStatus(@PathParam("status") PaymentStatus paymentStatus) {
        return invoiceService.findInvoiceByStatus(paymentStatus);
    }

    @GET
    @Secured
    @Path("customer/{customerId}")
    public Collection<Invoice> getInvoiceByCustomer(@PathParam("customerId") Long customerId) {
        return invoiceService.findInvoiceByCustomer(customerId);
    }

    @GET
    @Secured
    @Path("{invoiceId}/complaints/")
    public Response getInvoiceComplaints(@PathParam("invoiceId") Long invoiceId) {
        return Response.ok(invoiceComplaintService.getComplaints(invoiceService.findInvoiceById(invoiceId))).build();
    }

    /**
     * starts of an asynchronous process that recalculates an invoice
     * @param userId
     * @return
     */
    @POST
    @Path("/calculate/{userId}")
    public Response calculate(@PathParam("userId") Long userId) {
        Calendar now = Calendar.getInstance();
        Invoice preCalculated = this.invoiceService.findByUserIDAndDate(userId, (now.get(Calendar.MONTH) + 1), (now.get(Calendar.YEAR)));
        if (preCalculated == null) {
            this.invoiceService.calculateInvoice(userId);
            return Response.ok().build();
        }
        return Response.notModified().build();
    }

    /**
     * starts of an asynchronous process that recalculates an invoice
     * @param userId
     * @return
     */
    @POST
    @Path("/recalculate/{userId}")
    public Response recalculate(@PathParam("userId") Long userId) {
        Calendar now = Calendar.getInstance();
        Invoice preCalculated = this.invoiceService.findByUserIDAndDate(userId, (now.get(Calendar.MONTH) + 1), (now.get(Calendar.YEAR)));
        if(preCalculated != null) {
            this.invoiceService.recalculateInvoice(userId, preCalculated);
            return Response.ok().build();
        }
        return Response.noContent().build();
    }

    /**
     * starts of an asynchronous process that recalculates an invoice
     * @param invoiceId
     * @return
     */
    @POST
    @Path("/recalculate/id/{invoiceId}")
    public Response recalculateOnId(@PathParam("invoiceId") Long invoiceId) {
        Calendar now = Calendar.getInstance();
        Invoice preCalculated = this.invoiceService.findInvoiceById(invoiceId);
        if(preCalculated != null) {
            this.invoiceService.recalculateInvoice(preCalculated.getUserId(), preCalculated);
            return Response.ok().build();
        }
        return Response.noContent().build();
    }

    @POST
    @Secured
    public Response addInvoice(Invoice invoice, @Context UriInfo uriInfo) {
        try {
            Invoice i = invoiceService.addInvoice(invoice);
            i.setPaymentStatus(PaymentStatus.OPEN);
            i.setCreationDate(new Date());
            return Response.ok(i).build();
        } catch (NoResultException noResultException) {
            return Response.noContent().build();
        } catch (Exception exception) {
            return Response.serverError().build();
        }
    }

    @PUT
    @Secured
    public Response updateInvoice(Invoice invoice, @Context UriInfo uriInfo) {
        try {
            Invoice i = invoiceService.updateInvoice(invoice);
            return Response.ok(i).build();
        } catch (NoResultException noResultException) {
            return Response.noContent().build();
        } catch (Exception exception) {
            return Response.serverError().build();
        }
    }

    @DELETE
    @Secured
    @Path("{id}")
    public Response deleteInvoice(@PathParam("id") Long id, @Context UriInfo uriInfo) {
        try {
            Invoice invoice = invoiceService.findInvoiceById(id);
            invoiceService.deleteInvoice(invoice);
            return Response.ok(invoice).build();
        } catch (NoResultException noResultException) {
            return Response.noContent().build();
        } catch (Exception exception) {
            return Response.serverError().build();
        }
    }

    @POST
    @Path("calculateForeignersInvoices")
    public Response calculateForeignersInvoices() {
        this.invoiceService.calculateForeignerInvoices();
        return Response.ok().build();
    }
}
