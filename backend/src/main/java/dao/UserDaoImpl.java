package dao;

import dao.template.UserDao;
import domain.User;

import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Stateless
public class UserDaoImpl implements UserDao{
    @PersistenceContext
    EntityManager em;

    public int count() {
        return em.createNamedQuery("User.allUsers").getResultList().size();
    }

    public User create(User user) {
        em.persist(user);
        return user;
    }

    public User edit(User user) {
        em.merge(user);
        return user;
    }

    public User remove(User user) {
        em.remove(user);
        return user;
    }

    public Collection<User> findAll() {
        return em.createNamedQuery("User.allUsers").getResultList();
    }

    public User findById(Long id) {
        try {
            return (User) em.createNamedQuery("User.findUserById")
                    .setParameter("id", id).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    public User findByUsername(String username) {
        try {
        return (User)em.createNamedQuery("User.findUserByUsername")
                .setParameter("username", username).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }
}
