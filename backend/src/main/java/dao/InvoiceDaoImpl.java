package dao;

import dao.template.InvoiceDao;
import domain.Invoice;
import util.enumerations.PaymentStatus;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.Collection;

@Stateless
public class InvoiceDaoImpl implements InvoiceDao{

    @PersistenceContext
    EntityManager em;

    public long count() {
        return em.createNamedQuery("Invoice.count", long.class).getSingleResult();
    }

    public Invoice create(Invoice invoice) {
        em.persist(invoice);
        return invoice;
    }

    public Invoice edit(Invoice invoice) {
        em.merge(invoice);
        return invoice;
    }

    public Invoice remove(Invoice invoice) {
        em.remove(invoice);
        return invoice;
    }

    public Invoice findById(Long id) {
        return em.createNamedQuery("Invoice.findInvoiceById" , Invoice.class).setParameter("id", id).getSingleResult();
    }

    /**
     * return invoice based on userid and date
     * @return Invoice
     */
    public Invoice findByUserIDAndDate(String query){
        Invoice invoice;
        try{
            invoice = (Invoice) em.createNativeQuery(query, Invoice.class).getSingleResult();
        }catch (NoResultException ex){
            ex.printStackTrace();
            return null;
        }
        return invoice;
    }

    public Collection<Invoice> findAll() {
        return em.createNamedQuery("Invoice.allInvoices", Invoice.class).getResultList();
    }

    public Collection<Invoice> findByStatus(PaymentStatus paymentStatus) {
        return em.createNamedQuery("Invoice.findInvoiceByStatus", Invoice.class).setParameter("paymentStatus", paymentStatus).getResultList();
    }

    public Collection<Invoice> findByCustomerId(Long userId) {
        return em.createNamedQuery("Invoice.findInvoiceByUser", Invoice.class).setParameter("userId", userId).getResultList();
    }

    /**
     * Retrieves data dynamically and returns a invoice list.
     * @param query
     * @return Collection<Movement>
     */
    public Collection<Invoice> filterQuery(String query){
        return em.createNativeQuery(query, Invoice.class).getResultList();
    }
}
