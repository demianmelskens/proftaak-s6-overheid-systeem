package dao;

import dao.template.TimeIntervalDao;
import domain.TimeInterval;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Stateless
public class TimeIntervalDaoImpl implements TimeIntervalDao {
    @PersistenceContext
    EntityManager em;

    public int count() {
        return em.createNamedQuery("TimeInterval.allTimeInterval").getResultList().size();
    }

    public TimeInterval create(TimeInterval timeInterval) {
        em.persist(timeInterval);
        return timeInterval;
    }

    public TimeInterval edit(TimeInterval timeInterval) {
        em.merge(timeInterval);
        return timeInterval;
    }

    public TimeInterval remove(TimeInterval timeInterval) {
        em.remove(timeInterval);
        return timeInterval;
    }

    public Collection<TimeInterval> findAll() {
        return em.createNamedQuery("TimeInterval.allTimeInterval").getResultList();
    }

    public TimeInterval findById(Long id) {
        return (TimeInterval) em.createNamedQuery("TimeInterval.findTimeIntervalById")
                .setParameter("id", id).getSingleResult();
    }

    public TimeInterval findByName(String name) {
        return (TimeInterval) em.createNamedQuery("TimeInterval.findTimeIntervalByName")
                .setParameter("name", name).getSingleResult();
    }
}
