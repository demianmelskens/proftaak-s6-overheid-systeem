package dao;

import dao.template.RegionDao;
import domain.Region;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Stateless
public class RegionDaoImpl implements RegionDao {
    @PersistenceContext
    EntityManager em;

    public int count() {
        return em.createNamedQuery("Region.allRegions").getResultList().size();
    }

    public Region create(Region region) {
        em.persist(region);
        return region;
    }

    public Region edit(Region region) {
        em.merge(region);
        return region;
    }

    public Region remove(Region region) {
        em.remove(region);
        return region;
    }

    public Collection<Region> findAll() {
        return em.createNamedQuery("Region.allRegions").getResultList();
    }

    public Region findById(Long id) {
        return (Region) em.createNamedQuery("Region.findRegionById").setParameter("id", id).getSingleResult();
    }

    public Region findByName(String name) {
        return (Region)em.createNamedQuery("Region.findRegionByName").setParameter("name", name).getSingleResult();
    }
}
