package dao;

import dao.template.InvoiceComplaintDao;
import domain.Invoice;
import domain.InvoiceComplaint;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Stateless
public class InvoiceComplaintDaoImpl implements InvoiceComplaintDao {

    @PersistenceContext
    EntityManager em;

    /**
     * returns the complaint with id
     * @param id
     * @return InvoiceComplaint
     */
    @Override
    public InvoiceComplaint getComplaint(Long id) {
        return em.createNamedQuery("InvoiceComplaint.get", InvoiceComplaint.class).setParameter("id", id).getSingleResult();
    }

    /**
     * returns list of complaints for the invoice
     * @param invoice
     * @return Collection<InvoiceComplaint>
     */
    @Override
    public Collection<InvoiceComplaint> getComplaints(Invoice invoice) {
        return em.createNamedQuery("InvoiceComplaint.allInvoiceComplaints", InvoiceComplaint.class).setParameter("invoice", invoice).getResultList();
    }
}
