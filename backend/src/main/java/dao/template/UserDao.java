package dao.template;

import domain.User;

import java.util.Collection;

public interface UserDao {

    int count();

    User create(User user);

    User edit(User user);

    User remove(User user);

    Collection<User> findAll();

    User findById(Long id);

    User findByUsername(String username);
}
