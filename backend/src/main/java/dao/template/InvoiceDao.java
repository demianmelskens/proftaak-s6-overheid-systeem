package dao.template;

import domain.Invoice;
import util.enumerations.PaymentStatus;

import java.util.Collection;

public interface InvoiceDao {

    long count();

    Invoice create(Invoice invoice);

    Invoice edit(Invoice invoice);

    Invoice remove(Invoice invoice);

    Collection<Invoice> findAll();

    Invoice findById(Long id);

    Invoice findByUserIDAndDate(String query);

    Collection<Invoice> findByStatus(PaymentStatus status);

    Collection<Invoice> findByCustomerId(Long userId);

    Collection<Invoice> filterQuery(String query);
}