package dao.template;

import domain.Invoice;
import domain.InvoiceComplaint;

import java.util.Collection;

public interface InvoiceComplaintDao {

    InvoiceComplaint getComplaint(Long id);
    Collection<InvoiceComplaint> getComplaints(Invoice invoice);
}
