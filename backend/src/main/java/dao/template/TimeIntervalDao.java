package dao.template;

import domain.TimeInterval;

import java.util.Collection;

public interface TimeIntervalDao {

    int count();

    TimeInterval create(TimeInterval timeInterval);

    TimeInterval edit(TimeInterval timeInterval);

    TimeInterval remove(TimeInterval timeInterval);

    Collection<TimeInterval> findAll();

    TimeInterval findById(Long id);

    TimeInterval findByName(String name);
}
