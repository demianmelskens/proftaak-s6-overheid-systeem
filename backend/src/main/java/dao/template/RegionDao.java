package dao.template;

import domain.Region;

import java.util.Collection;

public interface RegionDao {

    int count();

    Region create(Region region);

    Region edit(Region region);

    Region remove(Region region);

    Collection<Region> findAll();

    Region findById(Long id);

    Region findByName(String name);
}
