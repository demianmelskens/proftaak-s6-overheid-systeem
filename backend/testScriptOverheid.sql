use government;
SET foreign_key_checks = 0;
TRUNCATE `government`.`invoice`;
TRUNCATE `government`.`region`;
TRUNCATE `government`.`timeinterval`;
TRUNCATE `government`.`user`;
SET foreign_key_checks = 1;
#Users
INSERT INTO USER(email, expire_date, name, password, username, address, city, zipcode, type)
VALUES("test@testmail.com", current_timestamp(), "TestUser", "f44a5459503dda025c918f7e7ce0f000fb3270952fb7a9f2d5d2db8d3d808f4f", "xXTestUserXx", "Somewhere 107", "Eindhoven", "3842TG", "EMPLOYEE");

INSERT INTO USER(email, expire_date, name, password, username, address, city, zipcode, type)
VALUES("test2@testmail.com", current_timestamp(), "TestUser2", "9c3b22d17588776c43cf9d2606dfb08e377577592b529dcf4c29379447930b01", "xXTestUser2Xx", "Somewhere 7", "Eindhoven", "3857LE", "EMPLOYEE");

INSERT INTO USER(email, expire_date, name, password, username, address, city, zipcode, type)
VALUES("test3@testmail.com", current_timestamp(), "TestUser3", "3c2554d986c88a53c11aee8fc75d9a6271866043b3849f57cf656676442629b0", "xXTestUser3Xx", "Somewhere 1407", "Eindhoven", "9174HF", "EMPLOYEE");

INSERT INTO USER(email, expire_date, name, password, username, address, city, zipcode, type)
VALUES("test4@testmail.com", current_timestamp(), "TestUser4", "df64f6f8a918153211fa340da865833c2473a077ac946c78940db039cd539ca7", "xXTestUser4Xx", "Somewhere 185", "Eindhoven", "8456QP", "EMPLOYEE");


#Invoices
INSERT INTO INVOICE(creation_date, status, payment_status, total_amount, user_id)
VALUES(current_timestamp(), "PENDING", "OPEN", "140.40", 1);

INSERT INTO INVOICE(creation_date, fulfilled_date, status, payment_status, total_amount, user_id)
VALUES(current_timestamp(), current_timestamp(), "SUCCESS", "FULFILLED", "540.43", 5);

INSERT INTO INVOICE(creation_date, status, payment_status, total_amount, user_id)
VALUES(current_timestamp(), "FAILED", "OPEN", "83.00", 14);

INSERT INTO INVOICE(creation_date, status, payment_status, total_amount, user_id)
VALUES(current_timestamp(), "WARNING", "OPEN", "1057.99", 97);

#InvoicesComplaints
INSERT INTO INVOICECOMPLAINT(complaint, invoice)
VALUES('das niet goed he', 1);

INSERT INTO INVOICECOMPLAINT(complaint, invoice)
VALUES('doe deze shit is ekkes opnieuw', 3);

#Regions
INSERT INTO REGION(basePrice, name, begin_lat, begin_lon, end_lat, end_lon)
VALUES(2.42, "Walcheren", "140.405643", "249.473629", "160.567983", "230.978432");

INSERT INTO REGION(basePrice, name, begin_lat, begin_lon, end_lat, end_lon)
VALUES(3.00, "Zeeland", "140.405643", "249.473629", "160.567983", "230.978432");

INSERT INTO REGION(basePrice, name, begin_lat, begin_lon, end_lat, end_lon)
VALUES(1.65, "Tongelre", "140.405643", "249.473629", "160.567983", "230.978432");

INSERT INTO REGION(basePrice, name, begin_lat, begin_lon, end_lat, end_lon)
VALUES(0.84, "Valkenswaard", "140.405643", "249.473629", "160.567983", "230.978432");

#Time Interval
INSERT INTO TIMEINTERVAL(multiplier, name, start_time, end_time, region_id)
VALUES(2.10, "Morning", current_timestamp(), current_timestamp(), 1);

INSERT INTO TIMEINTERVAL(multiplier, name, start_time, end_time, region_id)
VALUES(3.20, "Afternoon", current_timestamp(), current_timestamp(), 2);

INSERT INTO TIMEINTERVAL(multiplier, name, start_time, end_time, region_id)
VALUES(1.80, "Evening", current_timestamp(), current_timestamp(), 1);

INSERT INTO TIMEINTERVAL(multiplier, name, start_time, end_time, region_id)
VALUES(0.90, "Night", current_timestamp(), current_timestamp(), 2);