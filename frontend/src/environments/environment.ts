// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  regBaseUrl: 'http://192.168.24.21:8080/Register/api/',
  conBaseUrl: 'http://192.168.24.22:8080/Consumenten/api/',
  govBaseUrl: 'http://192.168.24.23:8080/Overheid/api/',
  polBaseUrl: 'http://192.168.24.24:8080/Politie/api/'
};
