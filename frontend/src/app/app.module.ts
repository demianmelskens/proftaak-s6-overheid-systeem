import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {StartComponent} from './components/pages/start/start.component';
import {LoginComponent} from './components/partials/login/login.component';
import {WarningBoxComponent} from './components/pieces/warning-box/warning-box.component';
import {AuthService} from './services/auth/auth.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProfileComponent} from './components/pages/profile/profile.component';
import {VehicleComponent} from './components/pages/vehicle/vehicle.component';
import {VehicleListComponent} from './components/pieces/vehicle-list/vehicle-list.component';
import {MapComponent} from './components/pieces/map/map.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {AgmCoreModule} from '@agm/core';
import { MenuComponent } from './components/pieces/menu/menu.component';
import {VehicleService} from './services/vehicle/vehicle.service';
import {MovementService} from './services/movement/movement.service';
import {InvoiceService} from './services/invoice/invoice.service';
import {CustomerService} from './services/customer/customer.service';
import {RegionService} from './services/region/region.service';
import {TimeIntervalService} from './services/time-interval/time-interval.service';
import { InvoiceComponent } from './components/pages/invoice/invoice.component';
import { ProfileListComponent } from './components/pieces/profile-list/profile-list.component';
import { InvoiceListComponent } from './components/pieces/invoice-list/invoice-list.component';
import { DateFormatPipe } from './pipes/date-format/date-format.pipe';
import { TaxComponent } from './components/pages/tax/tax.component';
import { RegionListComponent } from './components/pieces/region-list/region-list.component';
import { ProfileEditComponent } from './components/pages/profile-edit/profile-edit.component';
import { ProfileBoxComponent } from './components/pieces/profile-box/profile-box.component';
import { VehicleEditComponent } from './components/pages/vehicle-edit/vehicle-edit.component';
import { VehicleBoxComponent } from './components/pieces/vehicle-box/vehicle-box.component';


@NgModule({
  declarations: [
    AppComponent,
    StartComponent,
    LoginComponent,
    WarningBoxComponent,
    ProfileComponent,
    VehicleComponent,
    MapComponent,
    VehicleListComponent,
    MenuComponent,
    InvoiceComponent,
    ProfileListComponent,
    InvoiceListComponent,
    DateFormatPipe,
    TaxComponent,
    RegionListComponent,
    ProfileEditComponent,
    ProfileBoxComponent,
    VehicleEditComponent,
    VehicleBoxComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCjBqPJVSXL5mkiZU2IcXsqqmuRp1i-XMk'
    }),
    NgxPaginationModule,
  ],
  providers: [
    AuthService,
    VehicleService,
    MovementService,
    InvoiceService,
    CustomerService,
    RegionService,
    TimeIntervalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
