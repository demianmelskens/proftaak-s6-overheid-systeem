export class Location {

  public zipCode: string;
  public address: string;
  public city: string;

  constructor(zipCode: string, address: string, city: string) {
    this.zipCode = zipCode;
    this.address = address;
    this.city = city;
  }
}
