export class Vehicle {
  constructor(
    public id: number,
    public ownerId: number,
    public licensePlate: string,
    public registeredCountry: string,
    public brand: string,
    public model: string,
    public rateCategory: string,
  ) {}
}

