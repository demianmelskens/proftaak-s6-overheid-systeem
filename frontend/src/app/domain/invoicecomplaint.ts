import {Invoice} from './invoice';

export class Invoicecomplaint {

  constructor(public id: number,
              public complaint: string,
              public invoice: Invoice) {
  }
}
