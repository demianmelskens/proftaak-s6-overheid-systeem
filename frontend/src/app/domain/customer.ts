import {Vehicle} from './vehicle';
import {Invoice} from './invoice';
import {Location} from './location';

export class Consumer {

  public ownedVehicles: Vehicle[];
  public vehicleHistory: Vehicle[];
  public invoices: Invoice[];

  constructor(public id: number,
              public name: string,        // name of the user
              public username: string,        // name of the user
              public location: Location,    // location of the user
              public email: string,       // website link
              public password: string
  ) {
  }
}
