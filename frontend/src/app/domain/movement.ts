import {Position} from './position';

export class Movement {

  constructor(
    public serialNr: string,
    public position: Position,
    public timestamp: Date,
    public isMissed: boolean
  ) {
  }
}
