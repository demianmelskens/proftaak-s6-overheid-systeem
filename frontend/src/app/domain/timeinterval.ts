import {Region} from './region';

export class Timeinterval {
  public id: number;
  public name: string;
  public multiplier: number;
  public startTime: Date;
  public endTime: Date;
  public region: Region;

}
