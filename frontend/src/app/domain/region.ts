import {Timeinterval} from './timeinterval';


export class Region {

  public id: number;
  public name: string;
  public basePrice: number;
  public beginPoint: Position;
  public endPoint: Position;
  public timeIntervals: Timeinterval[];

  constructor() {
  }

  getZone() {
    return [this.beginPoint, this.endPoint];
  }
}
