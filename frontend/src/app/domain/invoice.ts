export class Invoice {

  public id: number;
  public userId: number;
  public totalAmount: number;
  public paymentStatus: string;
  public creationDate: Date;
  public fulfilledDate: Date;

  constructor () {

  }
}
