export class Position {

  constructor(
    public lat: number,
    public lon: number,
  ) {
  }
}
