import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StartComponent} from './components/pages/start/start.component';
import {LoginComponent} from './components/partials/login/login.component';
import {ProfileComponent} from './components/pages/profile/profile.component';
import {VehicleComponent} from './components/pages/vehicle/vehicle.component';
import {InvoiceComponent} from './components/pages/invoice/invoice.component';
import {TaxComponent} from './components/pages/tax/tax.component';
import {ProfileEditComponent} from './components/pages/profile-edit/profile-edit.component';
import {VehicleEditComponent} from './components/pages/vehicle-edit/vehicle-edit.component';

const routes: Routes = [
  {
    path: '',
    component: StartComponent,
    children: [
      {path: '', redirectTo: 'login', pathMatch: 'full'},
      {path: 'login', component: LoginComponent}
    ]
  },
  {path: 'profiles', component: ProfileComponent},
  {path: 'profiles/:profileid', component: ProfileEditComponent},
  {path: 'vehicles', component: VehicleComponent},
  {path: 'vehicles/:vehicleid', component: VehicleEditComponent},
  {path: 'invoices', component: InvoiceComponent},
  {path: 'taxes', component: TaxComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
