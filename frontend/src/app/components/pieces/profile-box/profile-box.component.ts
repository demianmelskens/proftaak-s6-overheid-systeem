import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Consumer} from '../../../domain/customer';

@Component({
  selector: 'app-profile-box',
  templateUrl: './profile-box.component.html',
  styleUrls: ['./profile-box.component.scss']
})
export class ProfileBoxComponent implements OnInit {

  @Input() customer: Consumer;
  @Input() edit: boolean;
  @Output() edited = new EventEmitter<Consumer>();

  constructor() {
  }

  ngOnInit() {
  }

  onSubmit(address: string, zipCode: string, city: string) {
    this.customer.location.address = address;
    this.customer.location.zipCode = zipCode;
    this.customer.location.city = city;
    this.edited.emit(this.customer);
  }

}
