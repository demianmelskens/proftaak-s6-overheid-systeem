import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Region} from '../../../domain/region';

@Component({
  selector: 'app-region-list',
  templateUrl: './region-list.component.html',
  styleUrls: ['./region-list.component.scss']
})
export class RegionListComponent implements OnInit {

  @Input() regions: Region[];
  @Output() clicked = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {
  }

  click(profileID: number) {
    this.clicked.emit(profileID);
  }
}
