import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Vehicle} from '../../../domain/vehicle';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.scss']
})
export class VehicleListComponent implements OnInit {

  @Input() vehicles: Vehicle[];
  @Output() clicked = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {
  }

  click(vehicleID: number) {
    this.clicked.emit(vehicleID);
  }

}
