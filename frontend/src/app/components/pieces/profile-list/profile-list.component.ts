import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Consumer} from '../../../domain/customer';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.scss']
})
export class ProfileListComponent implements OnInit {

  @Input() profiles: Consumer[];
  @Output() clicked = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  click(profileID: number) {
    this.clicked.emit(profileID);
  }
}
