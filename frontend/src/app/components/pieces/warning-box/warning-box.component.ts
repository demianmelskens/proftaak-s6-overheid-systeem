import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-warning-box',
  templateUrl: './warning-box.component.html',
  styleUrls: ['./warning-box.component.scss']
})
export class WarningBoxComponent implements OnInit {

  @Input() visible: boolean;
  @Input() text: string;

  constructor() {
  }

  ngOnInit() {
  }

  open(text) {
    console.log('open');
    if (text != null) {
      this.text = text;
    }
    this.visible = true;
  }

  close(event) {
    if (event != null) {
      event.preventDefault();
    }
    this.visible = false;
  }
}
