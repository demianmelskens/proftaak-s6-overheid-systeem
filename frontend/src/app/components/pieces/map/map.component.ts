import {Component, Input, OnInit} from '@angular/core';
import {Movement} from '../../../domain/movement';
import {Position} from '../../../domain/position';
import {Region} from '../../../domain/region';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  @Input() movements: Movement[];
  @Input() regions: Region[];
  @Input() startPos: Position;
  @Input() zoom: number;

  constructor() {
  }

  ngOnInit() {
    if (this.startPos == null) {
      this.startPos = new Position(53.340742, -7.842539);
    }
    if (this.zoom == null) {
      this.zoom = 6;
    }
  }
}
