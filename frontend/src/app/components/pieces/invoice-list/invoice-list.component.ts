import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Invoice} from '../../../domain/invoice';

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss']
})
export class InvoiceListComponent implements OnInit {

  @Input() invoices: Invoice[];
  @Output() recalculate = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {
  }

  click(profileID: number) {
    this.recalculate.emit(profileID);
  }
}
