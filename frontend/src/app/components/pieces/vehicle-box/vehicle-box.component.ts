import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Vehicle} from '../../../domain/vehicle';

@Component({
  selector: 'app-vehicle-box',
  templateUrl: './vehicle-box.component.html',
  styleUrls: ['./vehicle-box.component.scss']
})
export class VehicleBoxComponent implements OnInit {

  @Input() vehicle: Vehicle;
  @Input() edit: boolean;
  @Output() edited = new EventEmitter<Vehicle>();

  constructor() {
  }

  ngOnInit() {
  }

  onSubmit() {

  }

}
