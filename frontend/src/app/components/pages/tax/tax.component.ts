import {Component, OnInit} from '@angular/core';
import {Region} from '../../../domain/region';
import {Router} from '@angular/router';
import {RegionService} from '../../../services/region/region.service';

@Component({
  selector: 'app-tax',
  templateUrl: './tax.component.html',
  styleUrls: ['./tax.component.scss']
})
export class TaxComponent implements OnInit {

  regions: Region[];

  constructor(private router: Router, private regionService: RegionService) {
  }

  ngOnInit() {
    this.regionService.getAllRegions().subscribe(
      (data) => this.regions = data,
    (error) => console.log(error)
    );
  }

}
