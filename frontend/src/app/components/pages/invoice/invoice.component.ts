import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {InvoiceService} from '../../../services/invoice/invoice.service';
import {Invoice} from '../../../domain/invoice';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

  invoices: Invoice[];

  constructor(private router: Router, private invoiceService: InvoiceService) {
  }

  ngOnInit() {
    if (sessionStorage.getItem('token') == null) {
      this.router.navigate(['']);
    }
    this.invoiceService.getAll().subscribe(
      (data) => this.invoices = data,
      (error) => console.log(error)
    );
  }

  onRecalculate(invoiceId: number) {
    this.invoiceService.recalculate(invoiceId).subscribe(
      (data) => console.log(data),
      (error) => console.log(error)
    );
  }
}
