import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CustomerService} from '../../../services/customer/customer.service';
import {Consumer} from '../../../domain/customer';
import {InvoiceService} from '../../../services/invoice/invoice.service';
import {Invoice} from '../../../domain/invoice';
import {Vehicle} from '../../../domain/vehicle';
import {VehicleService} from '../../../services/vehicle/vehicle.service';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit {

  customer: Consumer;
  vehicles: Vehicle[];
  invoices: Invoice[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private customerService: CustomerService,
    private vehicleService: VehicleService,
    private invoiceService: InvoiceService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = +params['profileid'];
      this.customerService.getById(id).subscribe(
        (data) => this.customer = data,
        (error) => console.log(error)
      );
      this.vehicleService.getVehiclesByOwnerId(id).subscribe(
        (data) => this.vehicles = data,
        (error) => console.log(error)
      );
      this.invoiceService.getInvoicesOfCustomer(id).subscribe(
        (data) => this.invoices = data,
        (error) => console.log(error)
      );
    });
  }

  onEdit(customer: Consumer) {
    this.customerService.edit(customer).subscribe(
      (data) => this.customer = data,
      (error) => console.log(error),
      () => this.router.navigate(['/profiles'])
    );
  }

}
