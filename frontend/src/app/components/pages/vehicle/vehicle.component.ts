import {Component, OnInit} from '@angular/core';
import {VehicleService} from '../../../services/vehicle/vehicle.service';
import {MovementService} from '../../../services/movement/movement.service';
import {Vehicle} from '../../../domain/vehicle';
import {Movement} from '../../../domain/movement';
import {Router} from '@angular/router';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.scss']
})
export class VehicleComponent implements OnInit {

  vehicles: Vehicle[];
  movements: Movement[];

  constructor(private vehicleService: VehicleService, private movementService: MovementService, private router: Router) {
  }

  ngOnInit() {
    if (sessionStorage.getItem('token') == null) {
      this.router.navigate(['']);
    }
    this.vehicleService.getAll().subscribe(
      (data) => this.vehicles = data,
      (error) => console.log(error)
    );
  }

  onClick(vehicleID: number) {
    this.movementService.getAllMovementsByCarId(vehicleID).subscribe(
      (data) => this.movements = data,
      (error) => console.log(error)
    );
  }

}
