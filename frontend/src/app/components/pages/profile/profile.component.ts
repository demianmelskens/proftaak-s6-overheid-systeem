import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CustomerService} from '../../../services/customer/customer.service';
import {Consumer} from '../../../domain/customer';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  customers: Consumer[];

  constructor(private router: Router, private customerService: CustomerService) {
  }

  ngOnInit() {
    if (sessionStorage.getItem('token') == null) {
      this.router.navigate(['']);
    }
    this.customerService.getAll().subscribe(
      (data) => this.customers = data,
      (error) => console.log(error)
    );
  }

}
