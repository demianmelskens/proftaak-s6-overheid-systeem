import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CustomerService} from '../../../services/customer/customer.service';
import {Consumer} from '../../../domain/customer';
import {Vehicle} from '../../../domain/vehicle';
import {VehicleService} from '../../../services/vehicle/vehicle.service';
import {Movement} from '../../../domain/movement';
import {MovementService} from '../../../services/movement/movement.service';

@Component({
  selector: 'app-vehicle-edit',
  templateUrl: './vehicle-edit.component.html',
  styleUrls: ['./vehicle-edit.component.scss']
})
export class VehicleEditComponent implements OnInit {

  vehicle: Vehicle;
  movements: Movement[];
  owner: Consumer;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private vehicleService: VehicleService,
    private customerService: CustomerService,
    private movementService: MovementService
  ) {
  }

  ngOnInit() {
    console.log('vehicle edit');
    this.route.params.subscribe(params => {
      const id = +params['vehicleid'];
      this.vehicleService.getVehicleById(id).subscribe(
        (data) => this.vehicle = data,
        (error) => console.log(error),
        () => this.customerService.getById(this.vehicle.ownerId).subscribe(
          (data) => this.owner = data,
          (error) => console.log(error)
        )
      );
      this.movementService.getAllMovementsByCarId(id).subscribe(
        (data) => this.movements = data,
        (error) => console.log(error)
      );
    });
  }

  onEdit() {

  }

}
