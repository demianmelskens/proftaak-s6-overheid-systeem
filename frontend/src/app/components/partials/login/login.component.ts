import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth/auth.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  usernameError = false;
  passwordError = false;

  constructor(private authService: AuthService, private router: Router) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {
    if (sessionStorage.getItem('token') != null) {
      this.router.navigate(['profiles']);
    }
  }

  login(event, username, password) {
    event.preventDefault();
    if (this.loginForm.valid) {
      this.authService.login(username, password).subscribe(
        (data) => sessionStorage.setItem('token', data['token']),
        error => this.setError(error),
        () => this.router.navigate(['profiles'])
      );
    } else {
      this.setError('all');
    }
  }

  setError(value) {
    this.loginForm.reset();
    if (value === 'username') {
      this.usernameError = true;
    } else if (value === 'password') {
      this.passwordError = true;
    } else {
      this.usernameError = true;
      this.passwordError = true;
    }
  }

  resetError(value) {
    // this.loginForm.reset();
    if (value === 'username') {
      this.usernameError = false;
    } else if (value === 'password') {
      this.passwordError = false;
    } else {
      this.usernameError = false;
      this.passwordError = false;
    }
  }
}
