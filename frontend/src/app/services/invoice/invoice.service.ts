import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Invoice} from '../../domain/invoice';
import {Invoicecomplaint} from '../../domain/invoicecomplaint';

@Injectable()
export class InvoiceService {
  govBaseUrl = '';

  constructor(private http: HttpClient) {
    this.govBaseUrl = environment.govBaseUrl;
  }

  getAll() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<Invoice[]>(this.govBaseUrl + 'invoices', {headers: headers});
  }

  getInvoicesOfCustomer(customerId: number) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<Invoice[]>(this.govBaseUrl + 'invoices/customer/' + customerId, {headers: headers});
  }

  getInvoiceComplaints(invoiceId: number) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<Invoicecomplaint[]>(this.govBaseUrl + 'invoices/' + invoiceId + '/complaints/', {headers: headers});
  }

  recalculate(invoiceId: number) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.post(this.govBaseUrl + 'invoices/recalculate/id/' + invoiceId, {headers: headers});
  }
}
