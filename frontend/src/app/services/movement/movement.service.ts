import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Movement} from '../../domain/movement';

@Injectable()
export class MovementService {
  regBaseUrl = '';

  constructor(private http: HttpClient) {
    this.regBaseUrl = environment.regBaseUrl;
  }

  getAllMovementsByCarId(carId) {
    return this.http.get<Movement[]>(this.regBaseUrl + 'vehicles/' + carId + '/movements');
  }

  allMovementsByCarIDCurrentMonth(carId) {
    return this.http.get<Movement[]>(this.regBaseUrl + 'vehicles/' + carId + '/movements/month');
  }

  latestMovementByCarId(carId) {
    return this.http.get<Movement>(this.regBaseUrl + 'vehicles/' + carId + '/movements/latest');
  }

  getAll() {
    return this.http.get<Movement[]>(this.regBaseUrl + 'movements');
  }

  getMovementById(id) {
    return this.http.get<Movement>(this.regBaseUrl + 'movements/' + id);
  }
}
