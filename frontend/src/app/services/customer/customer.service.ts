import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Consumer} from '../../domain/customer';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class CustomerService {

  baseUrl = '';

  constructor(private http: HttpClient, private authService: AuthService) {
    this.baseUrl = environment.conBaseUrl + 'customers/';
  }

  edit(newConsumer: Consumer) {
    const token = sessionStorage.getItem('consumentToken');
    if (token == null) {
      this.authService.getTokenConsumenten().subscribe(
        (data) => sessionStorage.setItem('consumentToken', data[0]),
        (error) => console.log(error)
      );
    }
    if (token != null) {
      const headers = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token});
      return this.http.put<Consumer>(this.baseUrl, {
        'username': newConsumer.username,
        'email': newConsumer.email,
        'location': newConsumer.location,
        'id': newConsumer.id,
        'name': newConsumer.name,
        'password': newConsumer.password
      }, {headers: headers});
    }
  }

  getAll() {
    return this.http.get<Consumer[]>(this.baseUrl + 'all');
  }

  getById(id) {
    return this.http.get<Consumer>(this.baseUrl + id);
  }
}
