import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Region} from '../../domain/region';

@Injectable()
export class RegionService {

  govBaseUrl = '';

  constructor(private http: HttpClient) {
    this.govBaseUrl = environment.govBaseUrl + 'regions/';
  }

  getAllRegions() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<Region[]>(this.govBaseUrl, {headers: headers});
  }

  getRegionByid(id) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<Region>(this.govBaseUrl + id, {headers: headers});
  }

  getRegionByName(name) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<Region>(this.govBaseUrl + 'name/' + name, {headers: headers});
  }

  getTimeIntervalsForRegion(id) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<Region[]>(this.govBaseUrl + id + '/timeintervals', {headers: headers});
  }

  addRegion(region: Region) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.post(this.govBaseUrl, {region}, {headers: headers});
  }

  updateRegion(region: Region) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.put(this.govBaseUrl, {region}, {headers: headers});
  }

  deleteRegion(id) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.delete(this.govBaseUrl + id, {headers: headers});
  }
}
