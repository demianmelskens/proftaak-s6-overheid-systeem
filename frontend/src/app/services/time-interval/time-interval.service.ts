import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Timeinterval} from '../../domain/timeinterval';
import {Time} from '@angular/common';

@Injectable()
export class TimeIntervalService {

  govBaseUrl = '';

  constructor(private http: HttpClient) {
    this.govBaseUrl = environment.govBaseUrl + 'intervals/';
  }

  getAll() {
    return this.http.get<Timeinterval[]>(this.govBaseUrl);
  }

  getTimeIntervalById(intervalId: number) {
    return this.http.get<Timeinterval>(this.govBaseUrl + intervalId);
  }

  getTimeIntervalByName(name: string) {
    return this.http.get<Timeinterval>(this.govBaseUrl + 'name/' + name);
  }

  addTimeInterval(interval: Timeinterval) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.post(this.govBaseUrl, {interval}, {headers: headers});
  }

  updateTimeInterval(interval: Timeinterval) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.put(this.govBaseUrl, {interval}, {headers: headers});
  }

  deleteTimeInterval(id: number) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    this.http.delete(this.govBaseUrl + id, {headers: headers});
  }
}
