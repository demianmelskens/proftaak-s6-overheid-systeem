import {Injectable} from '@angular/core';
import {Vehicle} from '../../domain/vehicle';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable()
export class VehicleService {

  regBaseUrl = '';

  constructor(private http: HttpClient) {
    this.regBaseUrl = environment.regBaseUrl + 'vehicles/';
  }

  getAll() {
    return this.http.get<Vehicle[]>(this.regBaseUrl);
  }

  getAllWithPagination(offset: number, count: number) {
    return this.http.get<Vehicle[]>(this.regBaseUrl + 'offset/' + offset + '/count/' + count);
  }

  getVehicleById(id) {
    return this.http.get<Vehicle>(this.regBaseUrl + id);
  }

  getVehicleByLicensePlate(licensePlate: string) {
    return this.http.get<Vehicle>(this.regBaseUrl + 'licensePlate/' + licensePlate);
  }

  getVehiclesByOwnerId(ownerId) {
    return this.http.get<Vehicle[]>(this.regBaseUrl + 'owner/' + ownerId);
  }

  getVehiclesByListIds(idList) {
    const params = new HttpParams().set('vehicle', idList);
    return this.http.get<Vehicle[]>(this.regBaseUrl + 'idlist', {params: params});
  }

  getVehicleHistory(vehicleId) {
    return this.http.get(this.regBaseUrl + vehicleId + '/history');
  }

  getRateCategoryPrice(category: string) {
    return this.http.get(this.regBaseUrl + 'ratecategory/' + category);
  }

  addVehicle(vehicle: Vehicle) {
    return this.http.post(this.regBaseUrl, {vehicle});
  }

  sellVehicle(vehicleId: number, newOwner: number) {
    return this.http.put(this.regBaseUrl + vehicleId + '/sell/' + newOwner, {});
  }

  updateVehicle(vehicle: Vehicle) {
    return this.http.put(this.regBaseUrl, {vehicle});
  }
}
