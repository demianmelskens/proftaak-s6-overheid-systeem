import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient, private router: Router) {}

  login(username: string, password: string) {
    const headers = new HttpHeaders({'Content-Type': 'application/json', 'username': username, 'password': password});
    return this.http.post(
      environment.govBaseUrl + 'users/login',
      {},
      { headers: headers});
  }

  getTokenConsumenten() {
    const headers = new HttpHeaders({'Content-Type': 'application/json', 'username': 'jeroen', 'password': 'jeroen'});
    return this.http.post(
      environment.conBaseUrl + 'customers/login',
      {},
      { headers: headers});
  }

  getAuthUser() {
    const headers = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + sessionStorage.getItem('token')});
    return this.http.get(environment.govBaseUrl + 'customers/', { headers: headers});
  }
}
