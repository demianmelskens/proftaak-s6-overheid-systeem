import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value || value === undefined || value === null) return null;
    let s = value.toString().replace('T', ' ').toString();
    s = s.substring(0, s.indexOf('Z[UTC]'));
    return s;
  }

}
